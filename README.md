## Integration Pack UI Demo Application ##

The Integration Pack UI Demo provides a fully functional Web Application demonstrating using Boomi APIs to provision sub accounts, install and configure Integration Packs and then run Integration Processes provided by these Integration Packs.
### Prereqs ###
[Node.js](https://nodejs.org/en/)
The application is written in JavaScript and requires a running version of Node.js. You can download installers for OSX, Linux and Windows from here. Either version 4.x or 5.x is acceptable. 

***Note***: If you plan to run on Windows and you want to use https (the default) you will have to ensure openssl is installed. Check for binaries [here](https://slproweb.com/products/Win32OpenSSL.html). You must add a ***pathOpenSSL*** property to the configuration file that points to the path of where the openssl binrary was installed.

e.g
```
#!json

"pathOpenSSL": "/OpenSSL-Win32/bin/openssl"
```
Also, you will need to ensure the following environment variable is set in the environment to point to the corresponding openssl.cfg file
```
#!cmd

set OPENSSL_CONF=c:/OpenSSL-Win32/bin/openssl.cfg
```
### Boomi Account Setup ###
* The Integration Pack feature must be enabled in your Dell Boomi platform account. Contact your Dell Boomi representative to request this feature. Note that IPUI does not support multi-install integration packs.
* Account groups must be enabled in your Dell Boomi platform account. Contact your Dell Boomi representative to request this feature.
* A partner pricebook must exist for your account. The products and/or product bundles specified in the pricebook are used when provisioning new customer accounts. Contact your Dell Boomi representative to request a pricebook.
Setup at least one Account Group and assign at least one Integration Pack to it. Also ensure you have access to at least one Cloud instance.
* Your account must be configured by Dell Boomi as a PRM Admin user. PRM is an acronym for Partner Relationship Management. The Provision Partner Customer Account API endpoint accepts requests only from PRM Admin users. Contact your Dell Boomi representative to request this credential.

### Setup and Configuration ###
* Clone this repository. You should have an ***ipui*** directory
* cd into the ***ipui*** directory and run npm install
* In the ***ipui*** directory copy the config.template.json file to config.json
* Edit the config.json file with a text editor
```
#!json

{
    "cryptoPassword": "changemme",
    "userid": "boomi email address",
    "accountId": "",
    "baseUrl": "https://api.boomi.com",
    "sslPort": 9443,
    "password": "changeme",
    "debug": false,
    "accountGroupConfig": {
        "selfServiceId": "enter account group id",
        "enter account group id": {
            "environmentName": "enter environment name",
            "atomName": "enter atom name",
            "cloudId": "enter cloud id",
            "productCodes": [
                {  
                    "productCode": "enter product code",
                    "quantity": 1
                }
            ]
        }
    }
}

```

Enter your Boomi credentials for “userid”, “accountId” and “password”
Enter a value for “cryptoPassword”. This password is used for token encryption.
If you choose not to use https/ssl then change “sslPort” to “port” and set the value.

The “accountGroupConfig” details the account group setup that reflects the account groups you have created in your Boomi Account. It consists of a number of entries like this :


```
#!json

"enter account group id": {
    "environmentName": "enter environment name",
    "atomName": "enter atom name",
    "cloudId": "enter cloud id",
    "productCodes": [
        {  
            "productCode": "enter product code",
            "quantity": 1
        }
    ]
}
```
You can find your Account Group Ids and Cloud Ids by using the provided Boomi API tester tool ([here](https://bitbucket.org/boomi-community/jsapitest)) to query the AccountGroup and Cloud objects for your account. Enter the Id’s and Names for each account group you have set up. You can use any preferred Environment Name and Atom Name. These values will be used when the Environment and Atom is created for the sub-account.

Additionally if you want to support the self service mode, that is Partner customers have the Account Provisioning run when they first enter the application, then select an Account Group Id that you want to use for this and enter it for the “selfServiceId” value.

***Note***: The passwords entered are initially in plain text. When the application is first run the configuration file is re-written with the passwords encrypyted. A root password is used for this and can be found in /ipuidemo/ipui/lib/ipui.js


```
#!javascript

var rootPwd = "c7aluXMed";

```

Its is preferable that you change this to a password of your own choosing.
Running the Application

To start the application goto the ***ipui*** directory and run


```
#!cmd

node lib/ipui.js

```

You will see a console message :


```
#!cmd

Acme Integration Pack UI started on SSL port [9443]

```

### Admin Entry Point ###
The Admin UI is used to provision Accounts and generate URL’s for access to the main UI. You will have to login using your Boomi credentials. The userid and password must match the values stored in the config.json on the server.

* Point a browser at https://localhost:9443/devadmin.html for a debuggable version
* Point a browser at https://localhost:9443/admin.html for an optimized version

### Main UI Entry Point ###
Access to the Self Service Main UI is via :

* https://localhost:9443/ for an optimized version
* https://localhost:9443/devindex.html for a debuggable version

Users are presented with a button to create an Account. They will be prompted for the required account details and then the account provisioning will be started.

Access to the Main UI with a token. (This is the URL generated by the Admin UI)

* https://localhost:9443/#main/<token> for an optimized version
* https://localhost:9443/devindex.html#main/<token> for a debuggable version

Users are required to enter in the associated passphrase for the token.

### Proxy Service ###
Currently it is not possible to provision an Account and then create a new user that can be used to authenticate API calls for the newly created account. That leaves the only option as using the overrideAccount option with the partner API where necessary. While the BoomiAPI module can be used directly from browser based applications it would expose the Partners Account credentials to any customer creating and using an account. Because of that Version 1 of the application uses a proxy handler running on the server side to perform all Boomi API calls. 

Once the Boomi API has support for service user creation later version will enable both a direct and proxy mode.

### UI Architecture ###
The UI architecture is based on [AMD](https://github.com/amdjs/amdjs-api/wiki/AMD) using [RequireJS](http://requirejs.org/) as the loader implementation. All modules are written in AMD format.

Additionally a number of other packages/frameworks are leveraged:

* [jQuery](https://jquery.com/)
* [jQuery UI](https://jqueryui.com/) - Underlying UI framework 
* [Backbone.js](http://backbonejs.org/) - MVC like framework
* [Underscore.js](http://underscorejs.org/) - Templating Framework
* [Datatables](https://datatables.net/) - DataGrid jQuery UI plugin
* [jQuery Steps](http://www.jquery-steps.com/) - Wizard jQuery UI plugin
* [JSTree](https://www.jstree.com/) - Tree jQuery UI plugin

### WebSocket Usage ###
Some parts of the application make use of WebSockets. 

* Account Provisioning, both self-service and via Admin UI
* Process Execution Monitoring

### Customizing ###
#### Changing the Application Title and Image ####
To change the Application Title and Image modify this section in both ***/ipuidemo/ipui/resources/templates/main.html*** and ***/ipuidemo/ipui/resources/templates/admin.html***.

```
<div style="height: 80px;" class="content-head ui-widget-header">
	<table><tr>
	<td><img src="images/acme.png"></td>
	<td><h1>Acme Integrations</h1></td>
	</tr>
	</table>
</div>
```
Also change the follow section in ***/ipuidemo/ipui/resources/index.html*** and ***/ipuidemo/ipui/resources/admin.html*** to set the Title displayed in Browser Tabs

```
<!DOCTYPE html>
<html>
	<head>
		<title>Acme Integrations</title>
```

#### Removing Setup Wizard Panels ####
You can remove Setup Wizard panels by editing ***/ipuidemo/ipui/resources/templates/setup.html***. Each panel is specified as a set of h3/section html tags. To remove simply comment out the relevant tags. e.g 

```
        </section>
        <!--
        <h3>Properties</h3>
        <section>
          <h4>Properties:</h4>
	      <div>
	        <fieldset id="setupdynPropFields" class="schedule">
	        </fieldset>     
	      </div>       
        </section>
        -->
        <h3>Process Properties</h3>
        <section>
```

***Note***: For any UI changes make sure that the optimized versions of the apps JS code is built by running

```
grunt
```
in the ***/ipuidemo/ipui*** directory. A successful build result should look like

```
Running "requirejs:app" (requirejs) task

Running "requirejs:admin" (requirejs) task

Done, without errors.
```

#### AWS Elastic Beanstalk Deployment ####
Elastic Beanstalk provides a straightforward, simple deployment solution. When deployed with their load balancer option you can configure a domain name and certificate managed by their Route 53 service.

* Modify your ***config.json*** and remove both the ***sslPort*** and ***port*** option if they are specified.
* Run the ***eb_assemble.sh*** script to create an ***eb_ipui.zip*** file.
* When creating the Elastic Beanstalk application select ***Node.js*** as the platform and upload the ***eb_ipui.zip*** file.
* If you have selected the load balancer option then once the application has successfully deployed and started go to the ***Configuration -> Load Balancing*** section for the application and edit the configuration setting the Listener Protocol to ***TCP*** and the Secure Listener Protocol to ***SSL*** (this allows web socket connections to work correctly via the load balancer)
* Optionally select a ***SSL certificate ID*** to use with the application.
* If you have selected a ***SSL Certificate*** associate the load balancer instance to the domain via the Rout 54 Configuration panel.

#### Self-service Mode with Atom Install ####
If the main UI entry point is accessed without a token provided on the URL then IPUI enters its Self-service Mode. The user is given the option of either setting up an Atom on the configured Atom Cloud or to be able to install their own Atom on a machine of their choosing.
When this option is selected the Account Provisioning is split into multiple Steps

* When the Account Creation is initiated the provision of the account proceeds
* Once the provisioning step is complete the User is prompted to download a ***AtomInstaller.jar*** file and to run ***java -jar AtomInstaller.jar*** on their targeted machine (***Note*** the assumsuption is that the user has a Java 8 JRE installed on the target machine)
* The installer will make an HTTP callback request to notify the IPUI server that the installation has completed succuessfully or failed
* Once the IPUI server has been notified that the installer has finished the provisioning will continue to complete the setup steps and then allow the user to log in (if installation was successful).

#### AtomInstaller.jar ####
The ***AtomInstaller.jar*** file is stored in the resources directory of this repository. It is a simple Java application that when run :

* Determines the OS and Architecture (32bit/64bit) of the machine it is run on
* Downloads the correct Boomi Atom Installer from the Boomi Platform Server based on the determined OS/Architecture.
* Runs the installer 
* Makes a HTTP Request to the IPUI Server indicating success or failure

When a request to download the ***AtomInstaller.jar*** file is made a custom properties file is added to the jar file that contains :

* A Installer Token obtained via the Boomi API (in partner API mode https://api.boomi.com/partner/api/rest/v1/YOUR_ACCOUNT_ID/InstallerToken?overrideAccount=OVERRIDE_ACCOUNT_ID)
* A directory name for the installation
* The Account id
* The Callback URL for success or failure reporting

The source can be found at /AtomInstaller/src/com/boomi/ipui/atom/install/AtomInstaller.java

***Note:*** The IPUI server code will attempt to resolve the callback URL based on information about the machine it is running on. However, this may not resolve correctly, especially if a load balancer is in use. You can explicitly set the callback URL in the config.json file. 
e.g

```
"callbackUrl": "https://myipuiserver.com"
```
For example this property would need to be set if IPUI is deployed as an AWS Elastic Beanstalk application where a load balancing configuration is selected.
