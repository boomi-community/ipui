// Copyright (c) 2017 Boomi, Inc.

package com.boomi.ipui.atom.install;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AtomInstaller {
	private static final String OS_NAME = System.getProperty("os.name").toLowerCase();
	private static final String OS_ARCH = System.getProperty("os.arch").toLowerCase();
	
	private static final String ATOM_INSTALLER_URL_PREFIX = "https://platform.boomi.com/atom/";
	private static final String WINDOWS_32BIT_ATOM_INSTALLER_NAME = "atom_install.exe";
	private static final String WINDOWS_64BIT_ATOM_INSTALLER_NAME = "atom_install64.exe";
	private static final String LINUX_32BIT_ATOM_INSTALLER_NAME = "atom_install.sh";
	private static final String LINUX_64BIT_ATOM_INSTALLER_NAME = "atom_install64.sh";
	
	private static final String CURRENT_DIR = "./";
	private static final String CALLBACK_URL = "callbackUrl";
	private static final String DIRECTORY = "directory";
	private static final String TOKEN = "token";
	private static final String ACCOUNT_ID = "accountId";
	private static final String DISABLE_CERT_VALIDATION = "disableCertValidation";
	
	private static final String DOUBLE_QUOTE = "\"";
	private static final String ESCAPED_DOUBLE_QUOTE = "\\\"";
	private static final String BACKSLASH = "\\";
	private static final String ESCAPED_BACKSLASH = "\\\\";
	private static final String FALSE = "false";
	
	private static final String WIN = "win";
	private static final String MAC = "mac";
	private static final String NIX = "nix";
	private static final String NUX = "nux";
	private static final String AIX = "aix";
	
	private static final String CHMOD = "chmod";
	private static final String APLUSX = "a+x";
	
	private static final String X86 = "ProgramFiles(x86)";
	private static final String SIXTYFOUR = "64";
	
	private static final String DASH_Q = "-q";
	private static final String DASH_CONSOLE = "-console";
	private static final String INSTALL_TOKEN = "-VinstallToken=";
	private static final String ATOM_NAME = "-VatomName=IPUIAtom";
	private static final String DASH_DIR = "-dir ";
	
	private static final String POST = "POST";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String CONTENT_TYPE_JSON = "application/json";
	
	private static final String SSL = "SSL";
	
    private static final String JSON_TEMPLATE = "'{'\"accountId\": \"{0}\", \"returnCode\": {1}, \"errorMsg\": \"{2}\"'}'";
    private static final String INSTALL_PROPS_FILE = "atominstall.properties";

	public static void main(String[] args) {
		new AtomInstaller();
	}

	public AtomInstaller() {
		Properties installProps = loadInstallProps();
		String installerName = getInstallerName();
		ReadableByteChannel in = null;
		FileChannel out = null;
		FileOutputStream fos = null;
		boolean disableCertValidation = Boolean.valueOf(installProps.getProperty(DISABLE_CERT_VALIDATION, FALSE));

		try {
			URL url = new URL(ATOM_INSTALLER_URL_PREFIX+installerName);
			System.out.println("Downloading installer from "+ATOM_INSTALLER_URL_PREFIX+installerName);
			in = Channels.newChannel(url.openStream());
			fos = new FileOutputStream(installerName);
			out = fos.getChannel();
			out.transferFrom(in, 0, Long.MAX_VALUE);
			in.close();
			out.close();
			fos.close();
			if (isUnix() || isMac()) {
				changePermisions(installerName);
			}
			int rc = runInstaller(installerName, installProps.getProperty(DIRECTORY).replace(BACKSLASH, ESCAPED_BACKSLASH), installProps.getProperty(TOKEN));
			System.out.println("Making callback to "+installProps.getProperty(CALLBACK_URL));
			makeCallback(installProps.getProperty(CALLBACK_URL), installProps.getProperty(ACCOUNT_ID), rc, null, disableCertValidation);
		} catch(IOException e) {
			e.printStackTrace();
			makeCallback(installProps.getProperty(CALLBACK_URL), installProps.getProperty(ACCOUNT_ID), -1, e.getMessage().replace(DOUBLE_QUOTE, ESCAPED_DOUBLE_QUOTE), disableCertValidation);
		} catch (InterruptedException e) {
			e.printStackTrace();
			makeCallback(installProps.getProperty(CALLBACK_URL), installProps.getProperty(ACCOUNT_ID), -1, e.getMessage().replace(DOUBLE_QUOTE, ESCAPED_DOUBLE_QUOTE), disableCertValidation);
		} finally {
			if (in != null) { try { in.close(); } catch(IOException e) {}}	
			if (fos != null) { try { fos.close(); } catch(IOException e) {}}	
			if (out != null) { try { out.close(); } catch(IOException e) {}}	
		}
	}
	
	private static String getInstallerName() {
		if (isWindows()) {
			return is64Bit() ? WINDOWS_64BIT_ATOM_INSTALLER_NAME : WINDOWS_32BIT_ATOM_INSTALLER_NAME;
		} else if (isMac()) {
			return is64Bit() ? LINUX_64BIT_ATOM_INSTALLER_NAME : LINUX_32BIT_ATOM_INSTALLER_NAME;
		} else if (isUnix()) {
			return is64Bit() ? LINUX_64BIT_ATOM_INSTALLER_NAME : LINUX_32BIT_ATOM_INSTALLER_NAME;
		} else {
			return null;
		}
	}

	private static boolean isWindows() {
        return (OS_NAME.indexOf(WIN) >= 0);
    }

	private static boolean isMac() {
        return (OS_NAME.indexOf(MAC) >= 0);
    }

	private static boolean isUnix() {
        return (OS_NAME.indexOf(NIX) >= 0 || OS_NAME.indexOf(NUX) >= 0 || OS_NAME.indexOf(AIX) > 0 );
    }
    
	private static boolean is64Bit() {
    	boolean is64bit = false;
    	if (isWindows()) {
    	    is64bit = (System.getenv(X86) != null);
    	} else {
    	    is64bit = (OS_ARCH.indexOf(SIXTYFOUR) != -1);
    	}
    	return is64bit;
    }
	
	private void changePermisions(String installerName) throws IOException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder(CHMOD, APLUSX, installerName);
		Process p = pb.inheritIO().start();
		p.waitFor();
	}
	
	private int runInstaller(String installerName, String directory, String token) throws IOException, InterruptedException {
		String installerFileName = isWindows() ? installerName : CURRENT_DIR + installerName;
		ProcessBuilder pb = new ProcessBuilder(installerFileName, DASH_Q, DASH_CONSOLE, INSTALL_TOKEN+token, ATOM_NAME, DASH_DIR+directory);
		pb.inheritIO();
		System.out.print("Running installer command : ");
		for (String param : pb.command()) {
			System.out.print(param);
			System.out.print(" ");
		}
		System.out.println();
		Process p = pb.inheritIO().start();
		return p.waitFor();
	}
	
	private void makeCallback(String callbackUrl, String accountId, int rc, String errorMsg, boolean disableCertValidation) {
		if (disableCertValidation) {
			disableCertValidation();
		}
		try {
			URL url = new URL(callbackUrl);
			final HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setRequestMethod(POST);
	        httpConn.setDoInput(true);
	        httpConn.setDoOutput(true);
	        httpConn.setAllowUserInteraction(false);
	        httpConn.setInstanceFollowRedirects(false);
	        httpConn.setUseCaches(false);
	        
	        httpConn.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_JSON);
	        
	        final OutputStream outputStream = httpConn.getOutputStream();
	        String jsonString = MessageFormat.format(JSON_TEMPLATE, accountId, String.valueOf(rc), errorMsg == null ? "" : errorMsg);
	        outputStream.write(jsonString.getBytes());
	        outputStream.flush();
	        outputStream.close();
	        
	        final int responseCode = httpConn.getResponseCode();
	        
	        if (responseCode < 200 || responseCode >= 300) {
	        	System.out.println("Callback to "+callbackUrl+" failed : "+responseCode);	
	        } else {
	        	System.out.println("Callback to "+callbackUrl+" successful");	
	        }
		} catch (IOException e) {
        	System.out.println("Callback to "+callbackUrl+" failed : "+e.getMessage());
        	e.printStackTrace();
		}
	}
	
	private Properties loadInstallProps() {
		Properties installProps = new Properties();
		InputStream is = null;
		
		try {
			is = getClass().getClassLoader().getResourceAsStream(INSTALL_PROPS_FILE);
			installProps.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) { try { is.close(); } catch(IOException e) {}}	
		}
		return installProps;
	}
	
	/*
	 * The following enables HTTPS connections to servers that don't produce valid certificates.
	 * This is useful for testing purposes.
	 */
	private void disableCertValidation() {
		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	            public void checkClientTrusted(X509Certificate[] certs, String authType) {
	            }
	            public void checkServerTrusted(X509Certificate[] certs, String authType) {
	            }
	        }
	    };
	
		try {
			SSLContext sc = SSLContext.getInstance(SSL);
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());		
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
		    public boolean verify(String hostname, SSLSession sslSession) {
		        return true;
		    }
		});		
	}
}
