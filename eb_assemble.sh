#! /bin/bash
rm eb_ipui.zip
zip -r eb_ipui.zip config.json package.json lib/* resources/* .ebextensions/*
zip -d eb_ipui.zip *.DS_Store
zip -d eb_ipui.zip __MACOSX/\*
unzip -l eb_ipui.zip
