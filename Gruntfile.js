"use strict";

module.exports = function (grunt) {
    grunt.loadNpmTasks("grunt-contrib-requirejs");

    grunt.initConfig({
        requirejs: {
			app: {
    			options: {
      				baseUrl: "resources/js",
      				out: "resources/js/app-built.js",
      				optimize: "uglify2",
					paths: {
						BoomiAPI: '../../node_modules/BoomiAPI/js/BoomiAPI',
						APIUtils: '../../lib/APIUtils',
						jquery: '../lib/jquery/jquery-2.1.4',
						jqueryui: '../lib/jquery-ui',
						underscore: '../lib/underscore/underscore-1.8.3',
						backbone: '../lib/backbone/backbone-1.2.0',
						q: '../lib/q/q',
						datatables: '../lib/datatables/jquery.datatables',
						datatablesui: '../lib/datatables',
						jquerysteps: '../lib/jquery-steps/jquery.steps',
						jstree: '../lib/jstree/jstree',
						globalize: '../lib/globalize/globalize',
						cldr: '../lib/cldr/cldr',
						text: '../lib/requirejs/text',
						i18n: '../lib/requirejs/i18n',
						json: '../lib/requirejs/json',
						templates: '../templates',
						cldrdata: '../cldr-data'
					},
					shim: {
						'jquerystep': { 
							deps: ['jquery']
						}	
					},
					name: "app"
    			}
  			},
			admin: {
    			options: {
      				baseUrl: "resources/js",
      				out: "resources/js/adminapp-built.js",
      				optimize: "uglify2",
					paths: {
						BoomiAPI: '../../node_modules/BoomiAPI/js/BoomiAPI',
						APIUtils: '../../lib/APIUtils',
						jquery: '../lib/jquery/jquery-2.1.4',
						jqueryui: '../lib/jquery-ui',
						underscore: '../lib/underscore/underscore-1.8.3',
						backbone: '../lib/backbone/backbone-1.2.0',
						q: '../lib/q/q',
						datatables: '../lib/datatables/jquery.datatables',
						datatablesui: '../lib/datatables',
						jquerysteps: '../lib/jquery-steps/jquery.steps',
						jstree: '../lib/jstree/jstree',
						globalize: '../lib/globalize/globalize',
						cldr: '../lib/cldr/cldr',
						text: '../lib/requirejs/text',
						i18n: '../lib/requirejs/i18n',
						json: '../lib/requirejs/json',
						templates: '../templates',
						cldrdata: '../cldr-data'
					},
					shim: {
						'jquerystep': { 
							deps: ['jquery']
						}	
					},
					name: "adminapp"
    			}
  			}        
        }
    });

    grunt.registerTask("default", ["requirejs:app", "requirejs:admin"]);
};