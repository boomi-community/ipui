// Copyright (c) 2015 Dell Boomi, Inc.


(function(factory) {
	var root = (typeof self == 'object' && self.self == self && self) ||
      		   (typeof global == 'object' && global.global == global && global);
	// AMD
	if (typeof define === "function" && define.amd) {
		define(["BoomiAPI", "exports"], function(BoomiAPI, exports) {
			root.APIUtils = factory(root, exports, BoomiAPI);
		});
	// Node.js or CommonJS	
	} else if (typeof exports !== 'undefined') {
		var BoomiAPI = require("BoomiAPI");
		factory(root, exports, BoomiAPI);
	}
}(function(root, APIUtils, BoomiAPI) {
	
    var emptyFilter = {
        "QueryFilter" : {
            "expression" : {}
        }
    };
	
    var APIHandler = function(options) {
    	this.options = options;
    };
    
    APIHandler.prototype = {
    	getCredentials: function(accountId) {
			var creds = {
				baseUrl: this.options.baseUrl,
		 		userid: this.options.userid,
		 		password: this.options.password,
		 		accountId: this.options.accountId,
		 		debug: this.options.debug
			};
			if (accountId) {
				creds.accountId = accountId;
			}
			return creds;
	    },
		getAccount: function(d, cb, failCb) {
			var account = BoomiAPI.account(this.getCredentials(d.accountId));
			var environment = BoomiAPI.environment(this.getCredentials(d.accountId));
    		var integrationPack = BoomiAPI.integrationPack(this.getCredentials(d.accountId));
    		var integrationPackInstance = BoomiAPI.integrationPackInstance(this.getCredentials(d.accountId));

    		account.get({id: d.accountId})
			.then(function(results) {
				results.filter = emptyFilter;
				return environment.query(results);
			})
			.then(function(results) {
            	if (results.environments.numberOfResults > 0) {
            		results.environment = results.environments.result[0];
					results.filter = emptyFilter;
            		return integrationPack.query(results);
            	} else {
                	throw new Error("Unable to locate Environment");
            	}
			})
			.then(function(results) {
				results.filter = emptyFilter;
        		return integrationPackInstance.query(results);
			})
			.then(function(results) {
				var data = {
					account: results.account,
					environment: results.environment,
					available: results.integrationPacks.result,
					installed: results.integrationPackInstances.result
				};
				cb(data);
			})
			.fail(function(error) {
				if (error.message === "abort promise") {
					cb();
				} else {
					failCb(error);
				}
			});
		},
		findAccount: function(d, cb, failCb) {
			var filter = {
	            "QueryFilter" : {
					"expression" : {
			          "operator" : "and",
			          "nestedExpression" : [
			            {
			              "argument" : ["deleted"],
			              "operator" : "NOT_EQUALS",
			              "property" : "status"
			            },
			            {
		                    "operator" : "EQUALS",
		                    "property" : "name",
		                    "argument" : [d.accountName]
			            }
			          ]
			        }
	            }
			};
			
			var account = BoomiAPI.account(this.getCredentials());
			var environment = BoomiAPI.environment(this.getCredentials());
    		var integrationPack = BoomiAPI.integrationPack(this.getCredentials());
    		var integrationPackInstance = BoomiAPI.integrationPackInstance(this.getCredentials());
			
			account.query({filter: filter})
			.then(function(results) {
            	if (results.accounts.numberOfResults > 0) {
            		results.account = results.accounts.result[0];
					results.filter = emptyFilter;
					results.overrideAccountId = results.account.accountId;
					return environment.query(results);
				} else {
					throw new Error("abort promise");
				}
			})
			.then(function(results) {
            	if (results.environments.numberOfResults > 0) {
            		results.environment = results.environments.result[0];
					results.filter = emptyFilter;
            		return integrationPack.query(results);
            	} else {
                	throw new Error("Unable to locate Environment");
            	}
			})
			.then(function(results) {
				results.filter = emptyFilter;
        		return integrationPackInstance.query(results);
			})
			.then(function(results) {
				var data = {
					account: results.account,
					environment: results.environment,
					available: results.integrationPacks.result,
					installed: results.integrationPackInstances.result
				};
				cb(data);
			})
			.fail(function(error) {
				if (error.message === "abort promise") {
					cb();
				} else {
					failCb(error);
				}
			});
		},
		createAndRegisterAccount: function(d, cb, failCb) {
			var account = BoomiAPI.account(this.getCredentials());
			var environment = BoomiAPI.environment(this.getCredentials());
			var accountGroupAccount = BoomiAPI.accountGroupAccount(this.getCredentials());
			
			var accountData = {
				"name" : d.accountName,
				"expirationDate" : d.expirationDate
			};
			
			var environmentData = {
				"name" : d.environmentName
			};
			
			account.create({data: accountData, partner: true})
			.then(function(results) {
				results.overrideAccountId = results.account.accountId;
				results.data = environmentData;
				return environment.create(results);
			})
			.then(function(results) {
				var accountGroupAccountData = {
					"accountId" : results.account.accountId,
					"accountGroupId" : d.accountGroup.id				
				};
				results.overrideAccountId = undefined;
				results.data = accountGroupAccountData;
				return accountGroupAccount.create(results);
			})
			.then(function(results) {
				var data = {
					account: results.account,
					environment: results.environment
				};
				cb(data);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		createIntegrationPackInstance: function(d, cb, failCb) {
			var data = {
				integrationPackId: d.integrationPack.id
			};
			
			var integrationPackInstance = BoomiAPI.integrationPackInstance(this.getCredentials(d.account.accountId));
			var integrationPackEnvironmentAttachment = BoomiAPI.integrationPackEnvironmentAttachment(this.getCredentials(d.account.accountId));
			
			integrationPackInstance.create({data: data})
			.then(function(results) {
				var data = {
					"integrationPackInstanceId" : results.integrationPackInstance.id,
					"environmentId" : d.environment.id				
				};
				results.data = data;
				return integrationPackEnvironmentAttachment.create(results);
			})
			.then(function(results) {
				cb(results.integrationPackInstance);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		deleteIntegrationPackInstance: function(d, cb, failCb) {
			var integrationPackInstance = BoomiAPI.integrationPackInstance(this.getCredentials(d.account.accountId));
			integrationPackInstance.del({id: d.ipi.id})
			.then(function(results) {
				cb();
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		createAndAttachCloudAtom: function(d, cb, failCb) {
			var data = {
				cloudId : d.cloudId,
				name: d.atomName
			};
			
			var atom = BoomiAPI.atom(this.getCredentials());
			var environmentAtomAttachment = BoomiAPI.environmentAtomAttachment(this.getCredentials());
				
			atom.create({data: data, overrideAccountId: d.account.accountId})
			.then(function(results) {
				var data = {
					atomId : results.atom.id,
					environmentId: d.environment.id
				};
				
				results.data = data;
				return environmentAtomAttachment.create(results);
			})
			.then(function(results) {
				var data = {
					atom: results.atom,
					environmentAtomAttachment: results.environmentAtomAttachment
				};
				cb(data);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		getEnvironmentExtensions: function(d, cb, failCb) {
	        var filter = {
                "QueryFilter" : {
                    "expression" : {
                        "operator" : "EQUALS",
                        "property" : "environmentId",
                        "argument" : [d.environment.id]
                    }
                }
            };
	        
	        var environmentExtensions = BoomiAPI.environmentExtensions(this.getCredentials(d.account.accountId));
	        var environmentMapExtensionsSummary = BoomiAPI.environmentMapExtensionsSummary(this.getCredentials(d.account.accountId));
	        
	        environmentExtensions.query({filter: filter})
			.then(function(results) {
				return environmentMapExtensionsSummary.query(results);
			})
			.then(function(results) {
				var data = {
					mapExtensions: results.environmentMapExtensionsSummaries.result,
					extensions: results.environmentExtensions
				};
				cb(data);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		findProcesses: function(d, cb, failCb) {
			var process = BoomiAPI.process(this.getCredentials(d.account.accountId));
			
			process.query({filter: emptyFilter})
			.then(function(results) {
				cb(results.processes);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		getProcessDetails: function(d, cb, failCb) {
	        var filter = {
                "QueryFilter" : {
                    "expression" : {
                        "operator" : "EQUALS",
                        "property" : "processId",
                        "argument" : [d.processId]
                    }
                }
            };
			
	        var executionRecord = BoomiAPI.executionRecord(this.getCredentials(d.account.accountId));
	        
	        executionRecord.query({filter: filter})
			.then(function(results) {
				cb(results.executionRecords);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		runProcess: function(d, cb, failCb) {
			var atom = BoomiAPI.atom(this.getCredentials(d.account.accountId));
			var creds = this.getCredentials(d.account.accountId); 
			atom.query({filter: emptyFilter})
			.then(function(results) {
				var data = {
					atomId : results.atoms.result[0].id,
					processId: d.process.id
				};
				results.data = data;
				return BoomiAPI.executeProcess(creds, results);
			})
			.then(function(results) {
				cb();
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		saveEnvironmentExtensions: function(d, cb, failCb) {
			var environmentExtensions = BoomiAPI.environmentExtensions(this.getCredentials(d.account.accountId));
			environmentExtensions.update({id: d.extensions.id, data: d.extensions})
			.then(function(results) {
				cb();
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		getProcessSchedules: function(d, cb, failCb) {
			var processSchedules = BoomiAPI.processSchedules(this.getCredentials(d.account.accountId));
			var atom = BoomiAPI.atom(this.getCredentials(d.account.accountId));
			
			atom.query({filter: emptyFilter})
			.then(function(results) {
				var filter = {
		            "QueryFilter" : {
						"expression" : {
				          "operator" : "and",
				          "nestedExpression" : [
				            {
				              "argument" : [results.atoms.result[0].id],
				              "operator" : "EQUALS",
				              "property" : "atomId"
				            },
				            {
			                    "operator" : "EQUALS",
			                    "property" : "processId",
			                    "argument" : [d.process.id]
				            }
				          ]
				        }
		            }
				};
					
				results.filter = filter;
				return processSchedules.query(results);
			})
			.then(function(results) {
				cb(results.processSchedules);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		updateProcessSchedules: function(d, cb, failCb) {
			var processSchedules = BoomiAPI.processSchedules(this.getCredentials(d.account.accountId));
			
			processSchedules.update({id: d.schedules.id, data: d.schedules})
			.then(function(results) {
				cb();
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		findSubAccounts: function(d, cb, failCb) {
	        var filter = {
                "QueryFilter" : {
                    "expression" : {
                        "operator" : "EQUALS",
                        "property" : "accountGroupId",
                        "argument" : [d.accountGroup.id]
                    }
                }
            };
	        
	        var accountGroupAccount = BoomiAPI.accountGroupAccount(this.getCredentials());
	        var account = BoomiAPI.account(this.getCredentials());
	        
	        accountGroupAccount.query({filter: filter})
			.then(function(results) {
				var bulk = {
					"type" : "GET",
					"request" : []
				};
				results.accountGroupAccounts.result.forEach(function(accountGroupAccount) {
					bulk.request.push({id: accountGroupAccount.accountId});
				});
				results.overrideAccountId = undefined;
				results.filter = undefined;
				results.bulk = bulk;
				results.partner = true;
				return account.bulk(results);
			})
			.then(function(results) {
				var data = {
					accounts: [],
				};
				results.accounts.response.forEach(function(response) {
					if (response.statusCode === 200 && response.Result.status !== "deleted") {
						data.accounts.push(response.Result);
					}
				});
				
				cb(data);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		collectAdminData: function(d, cb, failCb) {
	        var filter = {
                "QueryFilter" : {
                    "expression" : {
                        "operator" : "NOT_EQUALS",
                        "property" : "name",
                        "argument" : ["All Accounts"]
                    }
                }
            };
			var accountGroup = BoomiAPI.accountGroup(this.getCredentials());
			var cloud = BoomiAPI.cloud(this.getCredentials());
			
			accountGroup.query({filter: filter})
			.then(function(results) {
				results.filter = emptyFilter;
				return cloud.query(results);
			})
			.then(function(results) {
				var data = {
					accountGroups: results.accountGroups.result,
					clouds: results.clouds.result
				};
				cb(data);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		getMapExtension: function(d, cb, failCb) {
			var environmentMapExtension = BoomiAPI.environmentMapExtension(this.getCredentials(d.account.accountId));
			
			environmentMapExtension.get({id: d.mapExtensionId})
			.then(function(results) {
				cb(results.environmentMapExtension);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		getMapExtensionWithBrowse: function(d, cb, failCb) {
			var environmentMapExtension = BoomiAPI.environmentMapExtension(this.getCredentials(d.account.accountId));
			var data = {
				id: d.mapExtensionId,
				Map: {
					BrowseSettings: {}
				}
			};
			if (d.srcBrowseFields.length > 0) {
				data.Map.BrowseSettings.SourceBrowse = {
					sessionId: "",
					BrowseFields: d.srcBrowseFields
				};
			}
			if (d.destBrowseFields.length > 0) {
				data.Map.BrowseSettings.DestinationBrowse = {
					sessionId: "",
					BrowseFields: d.destBrowseFields
				};
			}
			environmentMapExtension.execute({id: d.mapExtensionId, data: data})
			.then(function(results) {
				cb(results.environmentMapExtension);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		provisionAccount: function(d, cb, failCb) {
			var accountProvision = BoomiAPI.accountProvision(this.getCredentials());
			accountProvision.execute({data: d.accountProvison})
			.then(function(results) {
				cb(results.accountProvison);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		getProvisionAccountStatus: function(d, cb, failCb) {
			var accountProvision = BoomiAPI.accountProvision(this.getCredentials());
			accountProvision.get({id: d.accountProvison.id})
			.then(function(results) {
				cb(results.accountProvison);
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		saveMapExtension: function(d, cb, failCb) {
			var environmentMapExtension = BoomiAPI.environmentMapExtension(this.getCredentials(d.account.accountId));
			environmentMapExtension.update({id: d.environmentMapExtension.id, data: d.environmentMapExtension})
			.then(function(results) {
				cb();
			})
			.fail(function(error) {
				failCb(error);
			});
		},
		findAtom: function(d, cb, failCb) {
			var atom = BoomiAPI.atom(this.getCredentials(d.accountId));
	        var filter = {
				"QueryFilter" : {
					"expression" : {
					  "operator" : "and",
					  "nestedExpression" : [
						{
							"operator" : "EQUALS",
							"property" : "name",
							"argument" : [d.atomName]
						},
						{
			   				"argument" : ["ATOM"],
							"operator" : "EQUALS",
						   	"property" : "type"						
						}
					  ]
					}
				}
            };
			
			atom.query({filter: filter})
			.then(function(results) {
				cb(results.atoms.result[0].id);
			})
			.fail(function(error) {
				failCb(error);
			});
		}
	};
	
	APIUtils.create = function(options) {
		return new APIHandler(options);
	}

	return APIUtils;
}));
