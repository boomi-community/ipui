// Copyright (c) 2016 Dell Boomi, Inc.

var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var aes_cbc = 'aes-128-cbc';

module.exports = {
	encrypt: function(text, pw) {
		var cipher = crypto.createCipher(algorithm, pw);
		var crypted = cipher.update(text,'utf8','hex');
		crypted += cipher.final('hex');
		return crypted;
	},
	decrypt: function(text, pw) {
		var decipher = crypto.createDecipher(algorithm, pw);
		var decrypted = decipher.update(text,'hex','utf8');
		decrypted += decipher.final('utf8');
		return decrypted;
	},
	encryptAES_CBC: function(text, pw) {
		var sha1 = crypto.createHash('sha1');
		sha1.update(pw,'utf8','hex');
		var key = sha1.digest().slice(0, 16);
		var md5 = crypto.createHash('md5');
		md5.update(pw,'utf8','hex');
		var iv = md5.digest().slice(0, 16);
		var cipher = crypto.createCipheriv(aes_cbc, key, iv);
		var crypted = cipher.update(text,'utf8','base64');
		crypted += cipher.final('base64');
		return crypted;
	}
};