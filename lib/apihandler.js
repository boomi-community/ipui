// Copyright (c) 2016 Dell Boomi, Inc.

var ws = require('ws');
var APIUtils = require("./APIUtils");
var BoomiAPI = require("BoomiAPI");
var CryptoUtil = require("./cryptoutil");
var AccountProvision = require("./accountprovision");

function zeroPad(val) {
	val = val + "";
	if (val.length == 1) {
		val = "0" + val;
	}
	return val;
}

Handler = function(options, provisionQueue) {
	this.options = options;
	this.apiUtils = APIUtils.create(options);
	this.sockets = {};
	this.provisionQueue = provisionQueue;
}

Handler.prototype = {
	startWebSocketServer: function(server) {
		var wss = new ws.Server({server: server});
		wss.on('connection', function(ws) {
			var creds;
			var id = setInterval(function() {
				if (creds) {
					this._getStatus(creds, 
						function(status) {
							ws.send(JSON.stringify(status), function(){});
						},
						function(error) {
							console.log(error);
						}
					);
				} else {
					var provisionStatus = [];
					this.provisionQueue.forEach(function(provisioner, index, object) {
						var s = provisioner.getStatus();
						if (s) {
							provisionStatus.push(s);
							if (s.status === "READY" || s.status === "FAILED") {
								object.splice(index, 1);
							}
						}
					});
					if (provisionStatus.length > 0) {
						ws.send(JSON.stringify(provisionStatus), function(){});
					}
				}
  			}.bind(this), 2000);
			ws.on('message', function(message) {
				var msg = JSON.parse(message);
				if (msg.accountProvision) {
					if (msg.accountProvision.id) {
						this.provisionQueue.forEach(function(provisioner) {
							if (provisioner.accountProvision.id && provisioner.accountProvision.id === msg.accountProvision.id) {
								ws.send(JSON.stringify(provisioner.msg.accountProvision), function(){});
							}
						});
					} else {
						this.provisionQueue.push(AccountProvision(msg, this.options));
					}	
				} else if (msg.creds) {
					creds = msg.creds;
				}
			}.bind(this));
			ws.on('close', function() {
				creds = undefined;
				clearInterval(id);
			}.bind(this));  			
		}.bind(this));
	}, 
	handle: function(request, response, next) {
		if (this._validateCredentials(request, response)) {
			var path = request.url;
			if (path.charAt(0) == '/') {
				path = path.substring(1);
			}
			var segments = path.split("/");
			if (request.method === "POST") {
				this._handlePost(request, response, segments);
			}
		}
	},
	_handlePost: function(request, response, segments) {
		var postdata = '';
		request.on('data', function(data) { postdata += data; });
		request.on('end', function() {
			var data = JSON.parse(postdata);
			this.apiUtils[data.apiName](
				data.data,
				function(results) {
					if (!results) {
						results = {};
					}
					response.setHeader('Content-Type', 'application/json; charset=UTF-8');
					response.write(JSON.stringify(results));
					response.end();
				},
				function(err) {
					response.setHeader('Content-Type', 'application/json; charset=UTF-8');
					if (err.responseText) {
						try {
							var responseJSON = JSON.parse(err.responseText);
							err.message = responseJSON.message;
						} catch (err) {
							err.message = err.responseText;
						}
					}
					response.write(JSON.stringify({error: err}));
					response.end();
				}
			);
		}.bind(this));
	},
	_validateCredentials: function(request, response) {
		var authHeader = request.headers['authorization'];
		var xAuthToken = request.headers['x-auth-token'];
		var xAuthSecret = request.headers['x-auth-secret'];
		if (authHeader) {
			var split = authHeader.split(' ');
			var creds = new Buffer(split[1], 'base64').toString();
			var credsplit = creds.split(':');
			if (credsplit[0] === this.options.userid && credsplit[1] === this.options.password) {
				return true;
			}
		} else if (xAuthToken && xAuthSecret) {
        	decrypted = CryptoUtil.decrypt(xAuthToken, this.options.cryptoPassword);
			var values = decrypted.split(',');
			if (values[0] == xAuthSecret) {
				return true;
			}
		}
		response.statusCode = 401;
		response.statusMessage = "Authorization Failed";
		response.end();
		return false;
	},
	_getStatus: function(creds, cb, failCb) {
		var options = {};
		if (this.options.isDirect) {
			options.userid = creds.userid;
			options.password = creds.pwd;
			options.accountId = creds.accountId;
			options.baseUrl = creds.apiurl;	
		} else {
			options.userid = this.options.userid;
			options.password = this.options.password;
			options.accountId = creds.subAccountId;
			options.baseUrl = this.options.baseUrl;	
		}
		var event = BoomiAPI.event(options);
		var oneHourBack = new Date();
		oneHourBack.setTime(new Date().getTime() - (60*60));
		var oneHourBackStr = oneHourBack.getFullYear()+"-"+
		                     zeroPad(oneHourBack.getMonth()+1)+"-"+
		                     zeroPad(oneHourBack.getDate())+"T"+
		                     zeroPad(oneHourBack.getHours())+":"+
		                     zeroPad(oneHourBack.getMinutes())+":"+
		                     zeroPad(oneHourBack.getSeconds())+"Z";
		var filter = {
            "QueryFilter" : {
				"expression" : {
		          "operator" : "and",
		          "nestedExpression" : [
		            {
		              "argument" : ["process.execution"],
		              "operator" : "EQUALS",
		              "property" : "eventType"
		            },
		            {
	                    "operator" : "GREATER_THAN_OR_EQUAL",
	                    "property" : "eventDate",
	                    "argument" : [oneHourBackStr]
		            }
		          ]
		        }
            }
		};
		event.query({filter: filter})
		.then(function(results) {
			cb(results.events.result);
		})
		.fail(function(error) {
			failCb(error);
		});
	}
};

function createHandler(options, provisionQueue) {
	return new Handler(options, provisionQueue);
}

exports = module.exports = createHandler;
