// Copyright (c) 2016 Dell Boomi, Inc.

var CryptoUtil = require("./cryptoutil");
var fs = require('fs');
var ip = require('ip');
var qs = require('querystring');
var APIUtils = require("./APIUtils");
var BoomiAPI = require("BoomiAPI");
var JSZip = require("jszip");

AtomInstallHandler = function(options, provisionQueue) {
	this.options = options;
	this.creds = {
		userid: options.userid,
		password: options.password,
		accountId: options.accountId,
		baseUrl: options.baseUrl,
		debug: options.debug
	};
	this.apiUtils = APIUtils.create(this.creds);
	
	this.provisionQueue = provisionQueue;
	if (options.callbackUrl) {
		this.callbackUrl = options.callbackUrl;
	} else {
		this.callbackUrl = options.sslPort ? "https://" : "http://";
		this.callbackUrl += ip.address()+":";
		this.callbackUrl += options.sslPort || options.port || 3000;
	}
	this.callbackUrl +="/atominstall/";
}

AtomInstallHandler.prototype = {
	handle: function(request, response, next) {
		var path = request.url;
		if (path.charAt(0) == '/') {
			path = path.substring(1);
		}
		var segments = path.split("/");
		var accountId = segments[0];
		if (accountId.indexOf("?") != -1) {
			accountId = accountId.substring(0, accountId.indexOf("?"));
		}
		this.provisionQueue.forEach(function(provisioner, index, object) {
			if (provisioner.accountProvision.accountId && provisioner.accountProvision.accountId === accountId) {
				if (request.method === "GET") {
					if (provisioner.accountProvision.token) {
						this.writeJarFileResponse(provisioner.accountProvision.token, 
												  provisioner.msg.installDirectory, 
												  accountId, 
												  this.callbackUrl+accountId, 
												  response);
					} else {
						var installerToken = BoomiAPI.installerToken(this.creds);
						installerToken.create({data: {"installType": "ATOM", "durationMinutes": 1440}, overrideAccountId: accountId})
						.then(function(results) {
							//console.log(results.installerToken);
							provisioner.accountProvision.token = results.installerToken.token;
							this.writeJarFileResponse(provisioner.accountProvision.token, 
													  provisioner.msg.installDirectory, 
													  accountId, 
													  this.callbackUrl+accountId, 
													  response);
						}.bind(this))
						.fail(function(error) {
							response.statusCode = 500;
							response.statusMessage = error.message;
							response.end();
						});
					}	
				} else if (request.method === "POST") {
					var postdata = '';
					request.on('data', function(data) { postdata += data; });
					request.on('end', function() {
						var data = JSON.parse(postdata);
						if (data.returnCode === 0) {
							//provisioner.createEnvironmentTest();
							this.apiUtils.findAtom({atomName: "IPUIAtom", accountId: accountId},
								function(atomId) {
									provisioner.createEnvironment(atomId);
								}, 
								function(err) {
									provisioner.accountProvision.status = "FAILED";
									provisioner.accountProvision.error = error;
								}
							);
						} else {
							provisioner.accountProvision.status = "FAILED";
							if (data.errorMsg !== "") {
								provisioner.accountProvision.error = "Atom Installer failed : "+data.errorMsg;
							} else {
								provisioner.accountProvision.error = "Atom Installer failed with return code : "+data.returnCode;
							}
						}
						response.setHeader('Content-Type', 'application/json; charset=UTF-8');
						response.write("{}");
					}.bind(this));				
				}
			}
		}.bind(this));		
	},
	writeJarFileResponse: function(token, directory, accountId, callbackUrl, response) {
		fs.readFile("resources/AtomInstaller.jar", function(err, data) {
			if (err) {
				response.statusCode = 500;
				response.statusMessage = err.message;
				response.end();
				return;
			}
			JSZip.loadAsync(data).then(function(zip) {
				var props = "token="+token+"\n"+
							"directory="+directory+"\n"+
							"accountId="+accountId+"\n"+
							"callbackUrl="+callbackUrl+"\n"+
							"disableCertValidation=true\n";
				zip.file("atominstall.properties", props);
				response.writeHead(200, { 'Content-Type': 'application/java-archive', 'Content-Disposition': 'attachment; filename="AtomInstaller.jar' });
				zip
				.generateNodeStream({type:'nodebuffer',streamFiles:true})
				.pipe(response)
				.on('finish', function () {
					console.log(props);
				});	
			});
		});	
	}
};

function createHandler(options, provisionQueue) {
	return new AtomInstallHandler(options, provisionQueue);
}

exports = module.exports = createHandler;
	