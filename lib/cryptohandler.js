// Copyright (c) 2016 Dell Boomi, Inc.

var CryptoUtil = require("./cryptoutil");

CryptoHandler = function(options) {
	this.options = options;
}

CryptoHandler.prototype = {
	handle: function(request, response, next) {
		var path = request.url;
		if (path.charAt(0) == '/') {
			path = path.substring(1);
		}
		var segments = path.split("/");
		if (request.method === "GET") {
			this._handleGet(request, response, segments);
		} else if (request.method === "POST") {
			this._handlePost(request, response, segments);
		}
	},
	_validateCredentials: function(request, response) {
		var authHeader = request.headers['authorization'];
		if (authHeader) {
			var split = authHeader.split(' ');
			var creds = new Buffer(split[1], 'base64').toString();
			var credsplit = creds.split(':');
			if (credsplit[0] === this.options.userid && credsplit[1] === this.options.password) {
				return true;
			}
		}
		response.statusCode = 401;
		response.statusMessage = "Authorization Required";
		response.end();
		return false;
	},
	_handleGet: function(request, response, segments) {
		if (this._validateCredentials(request, response)) {
			response.setHeader('Content-Type', 'application/json; charset=UTF-8');
			response.write(JSON.stringify({
				baseUrl: this.options.baseUrl, 
				isDirect: this.options.isDirect, 
				debug: this.options.debug, 
				accountGroupConfig: this.options.accountGroupConfig
			}));
			response.end();
		}
	},
	_handlePost: function(request, response, segments) {
		var postdata = '';
		request.on('data', function(data) { postdata += data; });
		request.on('end', function() {
			var data = JSON.parse(postdata);
            if (data.token) {
            	decrypted = CryptoUtil.decrypt(data.token, this.options.cryptoPassword);
				var values = decrypted.split(',');
				if (values[0] == data.password) {
					var responseData = {
						accountId: values[1],
						baseUrl: this.options.baseUrl,
						isDirect: this.options.isDirect,
						debug: this.options.debug
					}
					if (this.options.isDirect) {
						responseData.partnerUserId = values[2];
						responseData.partnerPassword = values[3];
						responseData.partnerAccountId = values[4];
					}
					response.setHeader('Content-Type', 'application/json; charset=UTF-8');
					response.write(JSON.stringify(responseData));
				} else {
					response.statusCode = 403;
					response.statusMessage = "Passwords do not match";
				}
            } else if (this._validateCredentials(request, response)) {
            	var values = [];
                values[0] = data.password;
            	values[1] = data.accountId;
            	if (this.options.isDirect) {
            		values[2] = data.partnerUserId;
            		values[3] = data.partnerPassword;
            		values[4] = data.partnerAccountId;
            	}
            	var token = CryptoUtil.encrypt(values.join(','), this.options.cryptoPassword);
				response.setHeader('Content-Type', 'application/json; charset=UTF-8');
				response.write(JSON.stringify({token: token}));
            }
			response.end();
		}.bind(this));
	}
};

function createHandler(options) {
	return new CryptoHandler(options);
}

exports = module.exports = createHandler;
