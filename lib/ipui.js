// Copyright (c) 2016 Dell Boomi, Inc.

var https = require('https');
var http = require('http');
var pem = require('pem');
var fs = require('fs');
var connect = require('connect');
var CryptoHandler = require("./cryptohandler");
var APIHandler = require("./apihandler");
var CryptoUtil = require("./cryptoutil");
var AtomInstallHandler = require("./atominstallhandler");

var resourcecdir = fs.realpathSync(__dirname+"/../resources");
var boomiapidir = fs.realpathSync(__dirname+"/../node_modules/BoomiAPI/js");
var apiutilsdir = fs.realpathSync(__dirname);

var configFileName = process.argv.length > 2 ? process.argv[2] : "./config.json";
var options = JSON.parse(fs.readFileSync(configFileName));
var rootPwd = "c7aluXMed";
var provisionQueue = [];

if (options.pathOpenSSL) {
	/* 
		The options.pathOpenSSL property indicates that the app is being run on Windows.
		You must set this property to the path of the openssl binary	
		Also, you will need to ensure the following environment variable 
		is set in the environment to point to the corresponding openssl.cfg file

		set OPENSSL_CONF=c:/OpenSSL-Win32/bin/openssl.cfg
	*/
	
	pem.config({
		pathOpenSSL: options.pathOpenSSL
	});
}

options.rootPwd = rootPwd;

if (options.password) {
	var password = options.password;
	options.encryptedPassword = CryptoUtil.encrypt(options.password, options.cryptoPassword);
	options.cryptoPassword = CryptoUtil.encrypt(options.cryptoPassword, rootPwd);
	options.password = undefined;
	fs.writeFile(configFileName, JSON.stringify(options, null, '\t'), 'utf8', function(err) {
		if (err) throw err;
	});
	options.password = password;
	start();
} else {
	options.cryptoPassword = CryptoUtil.decrypt(options.cryptoPassword, rootPwd);
	options.password = CryptoUtil.decrypt(options.encryptedPassword, options.cryptoPassword);
	start();
}

function start() {
	options.isDirect = false;
	var cryptoHandler = CryptoHandler(options);
	var apiHandler = APIHandler(options, provisionQueue);
	var atomInstallHandler = AtomInstallHandler(options, provisionQueue);
	
	var app = connect()
		.use("/crypto", cryptoHandler)
		.use("/api", apiHandler)
		.use("/atominstall", atomInstallHandler)
		.use(connect.static(resourcecdir, {'index': ['index.html']}));
		//.use(connect.static(boomiapidir))
		//.use(connect.static(apiutilsdir));

	if (options.sslPort) {
		pem.createCertificate({days:1, selfSigned:true}, function(err, keys) {
			var server = https.createServer({key: keys.serviceKey, cert: keys.certificate}, app).listen(options.sslPort);
			apiHandler.startWebSocketServer(server);

			console.log("Acme Integration Pack UI started on SSL port ["+options.sslPort+"]");
		});
	} else if (options.port) {
		var server = http.createServer(app).listen(options.port);
		apiHandler.startWebSocketServer(server);

		console.log("Acme Integration Pack UI started on port ["+options.port+"]");
	} else {
		var port = process.env.PORT || 3000;
		var server = http.createServer(app).listen(port);
		apiHandler.startWebSocketServer(server);

		console.log("Acme Integration Pack UI started on port ["+port+"]");
	}
}