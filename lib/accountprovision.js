// Copyright (c) 2016 Dell Boomi, Inc.

var BoomiAPI = require("BoomiAPI");
var CryptoUtil = require("./cryptoutil");

AccountProvision = function(msg, defaults) {
	this.creds = {
		userid: defaults.userid,
		password: defaults.password,
		accountId: defaults.accountId,
		baseUrl: defaults.baseUrl,
		debug: defaults.debug
	};
	
	this.msg = msg;
	this.defaults = defaults;
	
	if (this.msg.selfService) {
		var accountGroupConfig = defaults.accountGroupConfig[defaults.accountGroupConfig.selfServiceId];
		this.msg.environmentName = accountGroupConfig.environmentName;
		this.msg.accountGroupId = defaults.accountGroupConfig.selfServiceId;
		this.msg.cloudId = accountGroupConfig.cloudId;
		this.msg.atomName = accountGroupConfig.atomName;
		this.msg.accountProvision.product = accountGroupConfig.productCodes;
	}
	
	this.provisioner = BoomiAPI.accountProvision(this._getCredentials());
	
	this.accountProvision = {
		name: msg.accountProvision.name,
		status: "STARTED"
	};
	/*
	console.log(JSON.stringify(this.msg, null, '\t'));
	setTimeout(function() {
		this.accountProvision.id = "FakeProvisioningId";
		this.accountProvision.status = "PENDING";
		setTimeout(function() {
			this.accountProvision.status = "COMPLETED";
			setTimeout(function() {
				this.accountProvision.name = this.msg.accountProvision.name;
				this.accountProvision.accountId = "LocalAtomInstallAccountTest-1234";
				this.accountProvision.status = "ACCOUNT_PROVISIONED";
			}.bind(this), 3000);
		}.bind(this), 3000);
	}.bind(this), 3000);
	
	setTimeout(function() {
		this.accountProvision.status = "PENDING";
		setTimeout(function() {
			this.accountProvision.status = "COMPLETED";
			setTimeout(function() {
				this.accountProvision.status = "ENV_CREATED";
				setTimeout(function() {
					this.accountProvision.status = "ATTACHED_TO_ACCOUNT_GROUP";
					setTimeout(function() {
						this.accountProvision.status = "ATOM_CREATED";
						setTimeout(function() {
							this.accountProvision.status = "READY";
						}.bind(this), 5000);
					}.bind(this), 5000);
				}.bind(this), 5000);
			}.bind(this), 5000);
		}.bind(this), 5000);
	}.bind(this), 5000);
	*/
	
	this.provisioner.execute({data: msg.accountProvision})
	.then(function(results) {
		this.accountProvision = results.accountProvision;
		this.accountProvision.name = this.msg.accountProvision.name;
		this._getProvisionAccountStatus();
	}.bind(this))
	.fail(function(error) {
		console.log(error);
		this.accountProvision.status = "FAILED";
		this.accountProvision.error = error;
	}.bind(this));
}

AccountProvision.prototype = {
	getStatus: function() {
		return this.accountProvision;
	},
	_completeProvision: function() {
		if (this.msg.selfService) {
			var values = [];
			values[0] = this.msg.passphrase;
			values[1] = this.accountProvision.accountId;
			this.accountProvision.token = CryptoUtil.encrypt(values.join(','), this.defaults.cryptoPassword);	
		}
		this.accountProvision.status = "READY";
	},
	createEnvironmentTest: function() {
		setTimeout(function() {
			this.accountProvision.status = "ENV_CREATED";
			setTimeout(function() {
				this.accountProvision.status = "ATTACHED_TO_ACCOUNT_GROUP";
				setTimeout(function() {
					var values = [];
					values[0] = this.msg.passphrase;
					values[1] = this.accountProvision.accountId;
					this.accountProvision.token = CryptoUtil.encrypt(values.join(','), this.defaults.cryptoPassword);
					this.accountProvision.status = "READY";
				}.bind(this), 3000);
			}.bind(this), 3000);
		}.bind(this), 3000);
	},
	createEnvironment: function(atomId) {
		var environment = BoomiAPI.environment(this._getCredentials());
		var accountGroupAccount = BoomiAPI.accountGroupAccount(this._getCredentials());
		var atom = BoomiAPI.atom(this._getCredentials());
		var environmentAtomAttachment = BoomiAPI.environmentAtomAttachment(this._getCredentials());
		
		var environmentData = {
			"name" : this.msg.environmentName
		};
	
		var accountGroupAccountData = {
			"accountId" : this.accountProvision.accountId,
			"accountGroupId" : this.msg.accountGroupId				
		};
		
		var cloudData = {
			cloudId : this.msg.cloudId,
			name: this.msg.atomName
		};
		
		environment.create({data: environmentData, overrideAccountId: this.accountProvision.accountId})
		.then(function(results) {
			this.accountProvision.status = "ENV_CREATED";
			results.overrideAccountId = undefined;
			results.data = accountGroupAccountData;
			return accountGroupAccount.create(results);
		}.bind(this))
		.then(function(results) {
			this.accountProvision.status = "ATTACHED_TO_ACCOUNT_GROUP";
			results.overrideAccountId = this.accountProvision.accountId;
			results.data = cloudData;
			if (atomId) {
				var data = {
					atomId : atomId,
					environmentId: results.environment.id
				};
				results.data = data;
				return environmentAtomAttachment.create(results);
			} else {
				results.data = cloudData;
				return atom.create(results)
			}	
		}.bind(this))
		.then(function(results) {
			if (atomId) {
				this._completeProvision();
			} else {
				this.accountProvision.status = "ATOM_CREATED";
				var data = {
					atomId : results.atom.id,
					environmentId: results.environment.id
				};
				results.data = data;
				return environmentAtomAttachment.create(results);
			}
		}.bind(this))
		.then(function(results) {
			this._completeProvision();
		}.bind(this))
		.fail(function(error) {
			console.log(error);
			this.accountProvision.status = "FAILED";
			this.accountProvision.error = error.message;
		}.bind(this));
	},
	_getProvisionAccountStatus: function() {
		this.provisioner.get({id: this.accountProvision.id})
		.then(function(results) {
			this.accountProvision = results.accountProvision;
			this.accountProvision.name = this.msg.accountProvision.name;
			
			if (this.accountProvision.status === "PENDING") {
				setTimeout(function() {
					this._getProvisionAccountStatus();
				}.bind(this), 5000);
			} else if (this.accountProvision.status === "COMPLETED") {
				if (this.msg.localAtom) {
					this.accountProvision.status = "ACCOUNT_PROVISIONED";
					return;
				}
				this.createEnvironment();
			}
		}.bind(this))
		.fail(function(error) {
			console.log(error);
			this.accountProvision.status = "FAILED";
			this.accountProvision.error = error.message;
		}.bind(this));
	},
	_getCredentials: function() {
		return {
			baseUrl: this.creds.baseUrl,
			userid: this.creds.userid,
			password: this.creds.password,
			accountId: this.creds.accountId,
			debug: this.creds.debug
		};
	}
}

function createAccountProvision(msg, defaults) {
	return new AccountProvision(msg, defaults);
}

exports = module.exports = createAccountProvision;
