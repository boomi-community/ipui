// Copyright (c) 2015 Dell Boomi, Inc.

define(['jquery', 'routers/adminrouter'], function($, router) {
	var uiRouter;
	
    $(document).ready(function() {
    	uiRouter = new router();
	});
	
	return {};
});
