// Copyright (c) 2015 Dell Boomi, Inc.

define(['jquery', 'routers/router'], function($, router) {
	var uiRouter;
	
    $(document).ready(function() {
    	uiRouter = new router();
	});
	
	return {};
});
