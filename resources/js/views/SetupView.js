// Copyright (c) 2015 Dell Boomi, Inc.

define([
		'jquery', 
		'backbone',
		'underscore',
		'../models/IntegrationPack',
		'../models/IntegrationPackInstance',
		'../util/MessageDialogUtil',
		'../util/DateTime',
		'globalize',
		'text!templates/setup.html',
		'text!templates/connections.html',
		'text!templates/dynproperties.html',
		'text!templates/processproperties.html',
		'text!templates/schedule.html',
		'text!templates/datetimedatavalues.html',
		'text!templates/numberdatavalues.html',
		'text!templates/browsefield.html',
		'json!cldrdata/main/en/ca-gregorian.json',
		'json!cldrdata/main/en/numbers.json',
		'json!cldrdata/supplemental/likelySubtags.json',
		'json!cldrdata/supplemental/timeData.json',
		'json!cldrdata/supplemental/weekData.json',		
		'jquerysteps',
		'jqueryui/selectmenu',
		'jqueryui/menu',
		'jqueryui/spinner',
		'jqueryui/resizable',
		'jqueryui/dialog',
		'jqueryui/effect-scale',
		'jqueryui/accordion',
		'jstree',
		'globalize/date'
		], 
function(
	$, 
	Backbone, 
	_, 
	IntegrationPack, 
	IntegrationPackInstance, 
	MessageDialogUtil, 
	DateTime, 
	Globalize, 
	template, 
	conntemplate, 
	dptemplate, 
	pptemplate, 
	scheduleTemplate,
	dateTimeTemplate,
	numberTemplate,
	browsefieldTemplate,
	enCaGregorian, 
	numbers, 
	likelySubtags, 
	timeData, 
	weekData
) {
	function zeroPad(num, places) {
	  var zero = places - num.toString().length + 1;
	  return Array(+(zero > 0 && zero)).join("0") + num;
	}
	
	function xpathToJS(xpath, extensions) {
		var nodes = {};
		var root = {
			id: "/root",
			text: "Root",
			children: []
		};
		xpath.forEach(function(node) {
			if (node.name != "") {
				var elements = node.xpath.split("/");
				var parent;
				var parentName = node.xpath.substring(0, node.xpath.lastIndexOf('/'));
				if (parentName === "") {
					parent = root;
				} else {
					parent = nodes[parentName];
				}
				elements.forEach(function(element) {
					if (element != "") {
						if (!nodes[node.xpath]) {
							nodes[node.xpath] = {
								id: node.xpath,
								text: node.name,
								children: []
							};
							parent.children.push(nodes[node.xpath]);
						}
					}
				});
			}
		});
		if (extensions) {
			var parent = nodes["/Record/Elements"];
			extensions.forEach(function(node) {
				if (parent) {
					parent.children.push({id: "/Record/Elements/"+node.name, text: node.name, children: []});
				} else {
					root.children.push({id: "/"+node.name, text: node.name, children: []});
				}
			});
		}
		return [root];
	}
	
	Globalize.load(
	    enCaGregorian,
	    numbers,
	    likelySubtags,
	    timeData,
	    weekData
	);	
    var en = Globalize( "en" );
    var dateFormatter = en.dateFormatter({ skeleton: "hm" });
    var dateParser = en.dateParser({ skeleton: "hm" });
	$.widget( "ui.timespinner", $.ui.spinner, {
	    options: {
	      step: 60 * 1000,
	      page: 60
	    },
	    _parse: function( value ) {
	    	if ( typeof value === "string" ) {
	            if ( Number( value ) == value ) {
	                return Number( value );
	            }	    		
	    		return +dateParser( value );
	        }
	        return value;
	    },
	    _format: function( value ) {
	    	return dateFormatter(new Date(value));
	    }	    
	});	
	var View = Backbone.View.extend({
		initialize: function(options) {
			this.template = _.template( template ) ( {href: this.href} );
			this.apiUtils = options.apiUtils;
			this.account = options.account;
			this.environment = options.environment;
			this.available = options.available;
			this.installed = options.installed;
		},
		render: function(){
			$(this.el).html(this.template);
			
		    this.$("#wizard").steps({
		        headerTag: "h3",
		        bodyTag: "section",
		        transitionEffect: "slideLeft",
		        stepsOrientation: "vertical",
		        autoFocus: true,
		        showFinishButtonAlways: true,
		        labels: {
		        	finish: "Save"
		        },
		        onFinished: function (event, currentIndex) {
		        	this.apiUtils.saveEnvironmentExtensions({account: this.account, extensions: this.updateExtensions()},
	    				function() {
		        			MessageDialogUtil.showMessageDialog("Save Complete", "Extensions Save is complete");
		        		},
						function(error) {
		        			MessageDialogUtil.showMessageDialog("Error", "Extensions Save failed : "+error.message);
						}
		        	);
		        }.bind(this),
		        onStepChanging: function (event, currentIndex, newIndex) {
		        	if (currentIndex != 0) {
		        		this.extensions = this.updateExtensions();
		        		if (newIndex == 4) {
		        			this.apiUtils.findProcesses({account: this.account},
	        					function(processes) {
	        						this.processes = processes;
	        		        		$('#setupprocs').DataTable().clear().draw();
	        						processes.result.forEach(function(process){
	        							$('#setupprocs').DataTable().row.add(process).draw(false);
	        						});
	        					}.bind(this),
	        					function(error) {
				        			MessageDialogUtil.showMessageDialog("Error", "Find Processes failed : "+error.message);
	        					}
	        				);
	        				if (this.mapExtensions && this.mapExtensions.length > 0) {
								this.selectMapExtension(0);
							}	
		        		}
		        		return true;
		        	}
		        	if (this.installed.length > 0) {
		        		$( "#tabs" ).tabs( "enable", 2 );	
		        		$( "#tabs" ).tabs( "enable", 3 );	

		    			this.apiUtils.getEnvironmentExtensions({account: this.account, environment: this.environment},
	    					function(results) {
		    					var extensions = results.extensions;
		    					this.mapExtensions = results.mapExtensions;
		    					this.extensions = extensions.result[0]; 
								this.connections = extensions.result[0].connections ? extensions.result[0].connections.connection : undefined;
								this.processProperties = extensions.result[0].processProperties ? extensions.result[0].processProperties.ProcessProperty : undefined;
								this.properties = extensions.result[0].properties ? extensions.result[0].properties.property : undefined;
								this.browseDialogs = {};
								$("#setupconnections").empty();
								$("#setupprocessprops").empty();
								$("#setupconnectionFields").empty();
								$("#setupprocessPropFields").empty();
								$("#setupdynPropFields").empty();
								$("#mapextensions").empty();
				        		$('#mappingsTable').DataTable().clear().draw();
				        		$('#functionsTable').DataTable().clear().draw();


								$( "#setupconnections" ).selectmenu({width: 200});
								$( "#setupprocessprops" ).selectmenu({width: 200});
								$( "#mapextensions" ).selectmenu({width: 200});
								if (this.connections) {
									var connectionOptions = [];
									//connectionOptions.push("<option value='none'></option>");
									this.connections.forEach(function(connection) {
										connectionOptions.push("<option value='"+connection.id+"'>"+connection.name+"</option>");
									});
									$( "#setupconnections" ).append(connectionOptions.join("")).selectmenu();
									$( "#setupconnections" ).selectmenu("refresh");
									if (this.connections.length > 0) {
										this.selectConnection(0);
									}
								}
								if (this.processProperties) {
									var ppOptions = [];
									//ppOptions.push("<option value='none'></option>");
									this.processProperties.forEach(function(processProperty) {
										ppOptions.push("<option value='"+processProperty.id+"'>"+processProperty.name+"</option>");
									});
									$( "#setupprocessprops" ).append(ppOptions.join(""));
									$( "#setupprocessprops" ).selectmenu("refresh");
									if (this.processProperties.length > 0) {
										this.selectProcessProperty(0);
									}
								}
								if (this.properties) {
									$("#setupdynPropFields").append(_.template( dptemplate ) ( {properties: this.properties} ));
								}
								
								if (this.mapExtensions) {
									var mapExtensionsOptions = [];
									this.mapExtensions.forEach(function(mapExtension) {
										mapExtensionsOptions.push("<option value='"+mapExtension.id+"'>"+mapExtension.name+"</option>");
										if (mapExtension.SourceFieldSet || mapExtension.DestinationFieldSet) {
											this.browseDialogs[mapExtension.id] = this.createBrowseDialog(mapExtension);
										}
									}.bind(this));
									$( "#mapextensions" ).append(mapExtensionsOptions.join("")).selectmenu();
									$( "#mapextensions" ).selectmenu("refresh");
								}
								
								$( "#setupconnections" ).selectmenu({
									change: function( event, ui ) {
										this.extensions = this.updateExtensions();
										$("#setupconnectionFields").empty();
										this.selectConnection(ui.item.index);
									}.bind(this)
								});
								
								$( "#setupprocessprops" ).selectmenu({
									change: function( event, ui ) {
										this.extensions = this.updateExtensions();
										$("#setupprocessPropFields").empty();
										this.selectProcessProperty(ui.item.index);
									}.bind(this)
								});
								
								$( "#mapextensions" ).selectmenu({
									change: function( event, ui ) {
										this.selectMapExtension(ui.item.index);
									}.bind(this)
								});
							}.bind(this),
							function(error) {
								console.log("error: "+error);
							}
						);
		        		return true; 
		        	} else {
		        		$( "#tabs" ).tabs( "disable", 2 );	
		        		$( "#tabs" ).tabs( "disable", 3 );	
		        		return false;
		        	}
		        }.bind(this)
		    });
		    
			setTimeout(function() {
				$('#setupips').DataTable( {
					columns: [
			            { "data": undefined, defaultContent: "", "width": "5em" },
			            { "data": "name", "width": "25em" },
			            { "data": "Description" }
			        ],		
					columnDefs: [ {
			            orderable: false,
			            className: 'select-checkbox',
			            targets:   0
			        } ],
			        select: {
			            style:    'single',
			            selector: 'td:first-child'
			        },
			        dom: 'frtip',
			        searching: false,
			        info: false
			    } );
				
				$('#setupinstalledips').DataTable( {
					columns: [
					    { "data": undefined, defaultContent: "", "width": "5em" },
			            { "data": "name", "width": "25em" },
			            { "data": "Description" }
			        ],		
					columnDefs: [ {
			            orderable: false,
			            className: 'select-checkbox',
			            targets:   0
			        } ],
			        select: {
			            style:    'single',
			            selector: 'td:first-child'
			        },
			        dom: 'frtip',
			        searching: false,
			        info: false
			    } );
				
				$('#setupprocs').DataTable( {
					columns: [
			            { "data": "name", "width": "40em" }
			        ],
			        select: {
			            style:    'single'
			        },
			        buttons: [
			            {
			            	extend: 'selected',
			                text: 'Save Schedule',
			                action: function ( e, dt, node, conf ) {
			                    var row = dt.row('.selected');
		                    	var process = row.data();
		                    	var schedules = this.updateSchedules(this.getSchedule());
		                    	this.apiUtils.updateProcessSchedules({account: this.account, process: process, schedules: schedules}, 
		                    		function() {
				        				MessageDialogUtil.showMessageDialog("Schedule Saved", "Schedule has been saved");
				        			},
				        			function(error) {
				        				MessageDialogUtil.showMessageDialog("Error", "Schedule Save failed : "+error.message);
				        			}
		                    	); 
			                }.bind(this)
			            }
			        ],
			        dom: 'Bfrtip',
			        searching: false,
			        info: false,
			        ordering: false,
			        pageLength: 4,
			        pagingType: "simple"
			    } );
				
				this.available.each(function(integrationPack) {
					if (this.installed.get(integrationPack.get("id")) === undefined) {
						$('#setupips').DataTable().row.add(integrationPack.toJSON()).draw(false);
					}
				}.bind(this));
				
				this.installed.each(function(integrationPackInstance) {
					var integrationPack = this.available.findWhere({id: integrationPackInstance.get("integrationPackId")});
					$('#setupinstalledips').DataTable().row.add(integrationPack.toJSON()).draw(false);
				}.bind(this));
				
				$('#setupips').DataTable()
			        .on( 'select', function ( e, dt, type, indexes ) {
			        	var selected = $('#setupips').DataTable().row(".selected").data();
			        	
	                	this.apiUtils.createIntegrationPackInstance({account: this.account, environment: this.environment, integrationPack: selected},
	                		function(result) {
	                			var integrationPackInstance = new IntegrationPackInstance(result);
	                			var integrationPack = this.available.findWhere({id: integrationPackInstance.get("integrationPackId")});
	                			this.available.remove(integrationPack);
	                			this.installed.add(integrationPackInstance);
								$('#setupinstalledips').DataTable().row.add(integrationPack.toJSON()).draw(false);
								$('#setupips').DataTable().rows('.selected').remove().draw();
	        		        	if (this.installed.length > 0) {
	        		        		$( "#tabs" ).tabs( "enable", 2 );	
	        		        		$( "#tabs" ).tabs( "enable", 3 );	
	        		        	} else {
	        		        		$( "#tabs" ).tabs( "disable", 2 );	
	        		        		$( "#tabs" ).tabs( "disable", 3 );	
	        		        	}
    							$( "#tabs" ).trigger( "ipuirefresh" );
	                		}.bind(this),
	                		function(error) {
		                		MessageDialogUtil.showMessageDialog("Error", "Integration Pack Add failed : "+error.message);
	                		}
	                	);
			        }.bind(this) );
				$('#setupinstalledips').DataTable()
			        .on( 'select', function ( e, dt, type, indexes ) {
			        	var selected = $('#setupinstalledips').DataTable().row(".selected").data();
			        	var integrationPackInstance = this.installed.findWhere({integrationPackId: selected.id});
	                	this.apiUtils.deleteIntegrationPackInstance({account: this.account, ipi: integrationPackInstance.toJSON()},
	                		function() {
								this.available.add(new IntegrationPack(selected));
								this.installed.remove(integrationPackInstance);
								$('#setupips').DataTable().row.add(selected).draw(false);
								$('#setupinstalledips').DataTable().rows('.selected').remove().draw();
	        		        	if (this.installed.length > 0) {
	        		        		$( "#tabs" ).tabs( "enable", 2 );	
	        		        		$( "#tabs" ).tabs( "enable", 3 );	
	        		        	} else {
	        		        		$( "#tabs" ).tabs( "disable", 2 );	
	        		        		$( "#tabs" ).tabs( "disable", 3 );	
	        		        	}
    							$( "#tabs" ).trigger( "ipuirefresh" );
	                		}.bind(this),
	                		function(error) {
		                		MessageDialogUtil.showMessageDialog("Error", "Integration Pack Remove failed : "+error.message);
	                		}
	                	);
			        }.bind(this) );
				
				$('#setupprocs').DataTable()
					.on( 'select', function ( e, dt, type, indexes ) {
			        	var process = $('#setupprocs').DataTable().row(".selected").data();
			        	this.apiUtils.getProcessSchedules({account: this.account, process: process},
		                	function(schedules) {
		                		if (schedules.numberOfResults > 0) {
				        			this.schedules = schedules.result[0];
				        		}
			        			$("#scheduleContainer").empty();
			        			var schedule = this.schedules && this.schedules.Schedule.length > 0 ? this.schedules.Schedule[0] : undefined; 
			        			var displaySchedule = this.setSchedule(schedule);
			        			$("#scheduleContainer").append(_.template( scheduleTemplate ) ( {schedule: displaySchedule} ));
			    				$("#interval").spinner({
			    					step: 1,
			    					max: 30,
			    					min: 0
			    				});
			    				$("#starttime").timespinner();
			    				$("#endtime").timespinner();
			    				$("#scheduletype").selectmenu({
			    					width: 100
			    				});
			    				$("#weekdays").buttonset();
			        		}.bind(this),
			        		function(error) {
		                		MessageDialogUtil.showMessageDialog("Error", "Process Schedules Get failed : "+error.message);
			        		}
			        	);		
			        }.bind(this) );
				
				$('#setupprocs').DataTable()
				.on( 'deselect', function ( e, dt, type, indexes ) {
					this.schedules = undefined;
        			$("#scheduleContainer").empty();
		        }.bind(this) );
				
				$('#mappingSelector').dialog({
			    	title: "Add a Mapping",
			        height: 320,
			        width: 500,
			        autoOpen: false,
			        show: {
			        	effect: "scale",
			        	duration: 500,
			        	percent: 100
			        },
			        hide: {
			        	effect: "scale",
			        	duration: 50,
			        	percent: 100
			        },
			        close: function() {
			        	this.selectedSrcNode = undefined;
			        	this.selectedDestNode = undefined;
			        }.bind(this)
			    });
				
				$('#mappingsTable').DataTable( {
					columns: [
			            { "data": "from", "width": "35em" },
			            { "data": "to" }
			        ],		
			        select: {
			            style: 'single'
			        },
			        buttons: [
			            {
			                text: 'Add',
			                action: function ( e, dt, node, conf ) {
								$( "#srctree" ).jstree('open_all');
								$( "#desttree" ).jstree('open_all');
								$('#mappingSelector').dialog("open");
			                }
			            },
			            {
			            	extend: 'selected',
			                text: 'Remove',
			                action: function ( e, dt, node, conf ) {
			                	var selected = dt.row( { selected: true } ).data();
			                	this.mapExtension.Map.ExtendedMappings.Mapping.forEach(function(mapping, index, object) {
									if (mapping.toFunction) {
				                		if (selected.mapping.fromXPath && selected.mapping.fromXPath === mapping.fromXPath) {
											object.splice(index, 1);
				                		}
									} else if (mapping.toFunction) {
				                		if (selected.mapping.toXPath && selected.mapping.toXPath === mapping.toXPath) {
											object.splice(index, 1);
				                		}
									} else {
				                		if (selected.mapping.toXPath && selected.mapping.toXPath === mapping.toXPath &&
				                		    selected.mapping.fromXPath && selected.mapping.fromXPath === mapping.fromXPath) {
											object.splice(index, 1);
				                		}
									}	
								});
								$('#mappingsTable').DataTable().rows('.selected').remove().draw();
			                }.bind(this)
			            }
			        ],
			        dom: 'Bfrtip',
			        searching: false,
			        info: false
			    } );
			    
				$('#srcDestExtTable').DataTable( {
					columns: [
			            { "data": "name", "width": "35em" },
			            { "data": "type" }
			        ],		
			        select: {
			            style: 'single'
			        },
			        buttons: [
			            {
			                text: 'Add Source Extension',
			                action: function ( e, dt, node, conf ) {
			                	this.currentExtensionType = "src";
			                	this.setExtensionValues();
								$('#addExtension').dialog("open");
			                }.bind(this)
			            },
			            {
			                text: 'Add Destination Extension',
			                action: function ( e, dt, node, conf ) {
			                	this.currentExtensionType = "dest";
			                	this.setExtensionValues();
								$('#addExtension').dialog("open");
			                }.bind(this)
			            },
			            {
			            	extend: 'selectedSingle',
			                text: 'Edit',
			                action: function ( e, dt, node, conf ) {
			                	var selected = dt.row( { selected: true } ).data();
			                	this.setExtensionValues(selected.ext);
			                	$('#addExtension').dialog("open");
			                }.bind(this)
			            },
			            {
			            	extend: 'selected',
			                text: 'Remove',
			                action: function ( e, dt, node, conf ) {
			                	var selected = dt.row( { selected: true } ).data();
			                	if (selected.type == "Source") {
									this.mapExtension.Map.SourceProfileExtensions.Node.forEach(function(node, index, object) {
										if (node.name === selected.name) {
											object.splice(index, 1);
										}
									});
			                	} else {
									this.mapExtension.Map.DestinationProfileExtensions.Node.forEach(function(node, index, object) {
										if (node.name === selected.name) {
											object.splice(index, 1);
										}
									});
			                	}
								$('#srcDestExtTable').DataTable().rows('.selected').remove().draw();
			                }.bind(this)
			            }
			        ],
			        dom: 'Bfrtip',
			        searching: false,
			        info: false
			    } );
				/*
				$('#functionsTable').DataTable( {
					columns: [
			            { "data": "id" },
			            { "data": "type" }
			        ],		
			        select: {
			            style: 'single'
			        },
			        
			        buttons: [
			            {
			                text: 'Add',
			                action: function ( e, dt, node, conf ) {
			                }
			            },
			            {
			            	extend: 'selectedSingle',
			                text: 'Edit',
			                action: function ( e, dt, node, conf ) {
			                }
			            },
			            {
			            	extend: 'selected',
			                text: 'Remove',
			                action: function ( e, dt, node, conf ) {
			                }
			            }
			        ],
			        dom: 'Bfrtip',
			        searching: false,
			        info: false
			    } );
				*/
				$("#map").button().click(function() {
					this.addMapping(this.selectedSrcNode.id, this.selectedDestNode.id);
					$('#mappingSelector').dialog("close");
				}.bind(this));
				
				$( "#map" ).button( "disable" );
				
				$("#mapFunction").button().click(function() {
					$('#mappingSelector').dialog("close");
				}.bind(this));
				
				$( "#mapFunction" ).button( "disable" );
				
				$( "#srctree" ).jstree({
					core: { 
						data: [],
						force_text: true,
						check_callback : function (operation, node, node_parent, node_position, more) {
				            return false;
				        }						
					}, 
					plugins : [ "dnd" ] 
				})
				.on('loaded.jstree', function() {
					$( "#srctree" ).jstree('open_all');
				})
				.on("select_node.jstree", function(evt, data){
					console.log("src selected : "+data.node.id);
					this.selectedSrcNode = data.node;
					this.isValidMap();
				}.bind(this));				
				
				$( "#desttree" ).jstree({
					core: { 
						data: [],
						force_text: true,
						check_callback : function (operation, node, node_parent, node_position, more) {
				            return operation === "copy_node" || operation === "move_node" || operation === "delete_node" ? true : false;
				        }						
					}, 
					plugins : [ "dnd" ] 
				})
				.on('loaded.jstree', function() {
					$( "#desttree" ).jstree('open_all');
				})
				.on("select_node.jstree", function(evt, data){
					console.log("dest selected : "+data.node.id);
					this.selectedDestNode = data.node;
					this.isValidMap();
				}.bind(this))				
				.on('copy_node.jstree', function (e, data) {
					var from = data.original.id;
					var to = data.node.parent;
					this.addMapping(from, to);
					$( "#desttree" ).jstree(true).delete_node(data.node);
					$('#mappingSelector').dialog("close");
				}.bind(this));

				$('#accordion').accordion({
				      collapsible: true,
				      heightStyle: "content"
			    });

				$('#addExtension').dialog({
			    	title: "Add an Extension",
			        height: 420,
			        width: 500,
			        autoOpen: false,
			        show: {
			        	effect: "scale",
			        	duration: 500,
			        	percent: 100
			        },
			        hide: {
			        	effect: "scale",
			        	duration: 50,
			        	percent: 100
			        },
			        buttons: {
			            Ok: function() {
			            	$('#addExtension').dialog( "close" );
			            	var ext = {
			            		name: $("#extName").val(),
			            		mandatory: $('#extMandatory').is(':checked'),
			            		enforceUnique: $('#extEnforceUnique').is(':checked'),
			            		fieldLengthValidation: $('#extFieldLengthValidation').is(':checked'),
			            		minLength: parseInt($('#extMinLen').val()),
			            		maxLength: parseInt($('#extMaxLen').val())
			            	};
			            	if (ext.name !== "") {
								var type = $('#extDataType').val();
								switch (type) {
									case "Character":
										ext.Character = {};
										break;
									case "Number":
										ext.Number = {
											format: $('#numberFormat').val(),
											signed: $('#numberFormatSigned').is(':checked'),
											impliedDecimal: parseInt($('#numberFormatImpliedDecimals').val())
										};
										break;
									case "Date/Time":
										ext.DateTime = {
											format: $('#dateFormat').val()
										}
										break;
								}
								console.log(ext);
								if (this.currentExtensionType === "src") {
									if (!this.mapExtension.Map.SourceProfileExtensions.Node) {
										this.mapExtension.Map.SourceProfileExtensions.Node = [];
									}
									this.mapExtension.Map.SourceProfileExtensions.Node.push(ext);
									$('#srcDestExtTable').DataTable().row.add({name: ext.name, type: "Source", ext: ext}).draw(false);
									$('#srctree').jstree(true).settings.core.data = xpathToJS(this.mapExtension.Map.SourceProfile.Node, this.mapExtension.Map.SourceProfileExtensions.Node);
								    $('#srctree').jstree(true).refresh();
								} else if (this.currentExtensionType === "dest") {
									if (!this.mapExtension.Map.DestinationProfileExtensions.Node) {
										this.mapExtension.Map.DestinationProfileExtensions.Node = [];
									}
									this.mapExtension.Map.DestinationProfileExtensions.Node.push(ext);
									$('#srcDestExtTable').DataTable().row.add({name: ext.name, type: "Destination", ext: ext}).draw(false);
									$('#desttree').jstree(true).settings.core.data = xpathToJS(this.mapExtension.Map.DestinationProfile.Node, this.mapExtension.Map.DestinationProfileExtensions.Node);
								    $('#desttree').jstree(true).refresh();
								}
			            	}
			            }.bind(this),
			            Cancel: function() {
			            	$( this ).dialog( "close" );
			            }
			        },
			        close: function() {
			        }.bind(this)
			    });
			    
				$("#extDataType").selectmenu({width: 150});
				$("#extDataType").selectmenu({
					change: function( event, ui ) {
						$("#dataFormatOptions").empty();
						switch (ui.item.value) {
							case "Character":
								$("#dateFormat").selectmenu( "destroy" );
								break;
							case "Number":
								$("#dateFormat").selectmenu( "destroy" );
								$("#dataFormatOptions").append(_.template( numberTemplate ) ( {} ));
								$('#numberFormatImpliedDecimals').spinner({step: 1, max: 100, min: 0});
								break;
							case "Date/Time":
								$("#dataFormatOptions").append(_.template( dateTimeTemplate ) ( {} ));
								$("#dateFormat").selectmenu();
								break;
						}
					}.bind(this)
				});
				
				$("#saveExt").button().click(function() {
					this.saveMapExtension();
				}.bind(this));

			}.bind(this), 500);
		},
		updateExtensions: function() {
			var extensions = this.extensions;
			var saveExtensions = {
				id: extensions.id,
				environmentId: extensions.environmentId,
				extensionGroupId: extensions.extensionGroupId,
				connections: {
					connection: []
				},
				processProperties: {
					ProcessProperty: []
				},
				properties: {
					property: []
				}
			};
			if (extensions.connections) {
				extensions.connections.connection.forEach(function(connection) {
					var saveConnection = {
						id: connection.id,
						name: connection.name,
						field: []
					};
					connection.field.forEach(function(field) {
						var overridden = $("#cb-"+connection.id+"-"+field.id).is(':checked');
						var saveField = {
							id: field.id,
							encryptedValueSet: field.encryptedValueSet,
							usesEncryption: field.usesEncryption,
							componentOverride: overridden,
							value: field.value || undefined
						};
						var value = $("#"+connection.id+"-"+field.id).val();
						if (overridden) {
							//console.log("c:"+saveField.id+" "+value);
							saveField.value = value;
						}
						saveConnection.field.push(saveField);
					});
					saveExtensions.connections.connection.push(saveConnection);
				});
			}
			if (extensions.processProperties) {
				extensions.processProperties.ProcessProperty.forEach(function(processProperty) {
					var saveProcessProperty = {
						id: processProperty.id,
						name: processProperty.name,
						ProcessPropertyValue: []
					};
					processProperty.ProcessPropertyValue.forEach(function(processPropertyValue) {
						var overridden = $("#cb-"+processPropertyValue.key).is(':checked');
						var saveProcessPropertyValue = {
							label: processPropertyValue.label,
							key: processPropertyValue.key,
							value: processPropertyValue.value || "",
							encryptedValueSet: processPropertyValue.encryptedValueSet
						};
						var value = $("#"+processPropertyValue.key).val();
						if (overridden) {
							//console.log("pp:"+processPropertyValue.key+" "+value);
							saveProcessPropertyValue.value = value; 
						} else {
							saveProcessPropertyValue.value = undefined; 
						}
						saveProcessProperty.ProcessPropertyValue.push(saveProcessPropertyValue);
					});
					saveExtensions.processProperties.ProcessProperty.push(saveProcessProperty);
				});
			}
			if (extensions.properties) {
				extensions.properties.property.forEach(function(property) {
					var saveProperty = {
						name: property.name,
						value: $("#"+property.name.replace(/\s/g, '_')).val()
					}
					//console.log("dp:"+saveProperty.name+" "+saveProperty.value);
					saveExtensions.properties.property.push(saveProperty);
				});
			}
			return saveExtensions;
		},
		getSchedule: function() {
			var startTime = $("#starttime").val();
			var startTimeParts = startTime.split(":");
			var startHour = startTimeParts[0];
			var startMinutes = startTimeParts[1];
			var endTime = $("#endtime").val();
			var endTimeParts = endTime.split(":");
			var endHour = endTimeParts[0];
			var endMinutes = endTimeParts[1];
			var interval = $("#interval").val();
			var type = $("#scheduletype").val();
			var sun = $("#sunday").prop('checked');
			var mon = $("#monday").prop('checked');
			var tue = $("#tuesday").prop('checked');
			var wed = $("#wednesday").prop('checked');
			var thu = $("#thursday").prop('checked');
			var fri = $("#friday").prop('checked');
			var sat = $("#saturday").prop('checked');
			
			var schedule = {};
			schedule.daysOfMonth = "1-31/1";
			if (type === "minute") {
				schedule.hours = parseInt(startHour) + "-" + parseInt(endHour);
				schedule.minutes = "0-59/" + interval;
				schedule.daysOfMonth = "*";
			} else if (type === "hour") {
				schedule.hours = parseInt(startHour) + "-" + parseInt(endHour)+"/"+interval;
				schedule.minutes = 0;
			} else if (type === "day") {
				schedule.hours = parseInt(startHour);
				schedule.minutes = 0;
				schedule.daysOfMonth = "1-31/"+interval;
			}
			schedule.months = "*";
			schedule.years = "*";
			schedule.daysOfWeek = sun === true ? "1," : "";
			schedule.daysOfWeek += mon === true ? "2," : "";
			schedule.daysOfWeek += tue === true ? "3," : "";
			schedule.daysOfWeek += wed === true ? "4," : "";
			schedule.daysOfWeek += thu === true ? "5," : "";
			schedule.daysOfWeek += fri === true ? "6," : "";
			schedule.daysOfWeek += sat === true ? "7," : "";
			schedule.daysOfWeek = schedule.daysOfWeek.substring(0, schedule.daysOfWeek.length-1);
			
			return schedule;
		},
		setSchedule: function(schedule) {
			var displaySchedule = {
				sunChecked: "",
				monChecked: "",
				tueChecked: "",
				wedChecked: "",
				thuChecked: "",
				friChecked: "",
				satChecked: "",
				minuteSelected: "",
				hourSelected: "",
				daySelected: "",
				interval: "15",
				startTime: "08:00 AM",
				endTime: "06:59 PM"
			};
			if (schedule) {
	            var day = schedule.daysOfMonth;
	            var dayParts = day.split("/");
	            
	            if (dayParts.length > 1) {
	            	displaySchedule.interval = parseInt(dayParts[1]);
	                displaySchedule.daySelected = "selected";
	                displaySchedule.minuteSelected = "";
	                displaySchedule.hourSelected = "";
	            }
	            
	            var hour = schedule.hours+"";
	            var hourParts = hour.split("/");
	            var hours = hourParts[0].split("-");
	            
	            if (hourParts.length > 1) {
	            	displaySchedule.interval = parseInt(hourParts[1]);
	                displaySchedule.hourSelected = "selected";
	                displaySchedule.daySelected = "";
	                displaySchedule.minuteSelected = "";
	            }
	            
	            var minute = schedule.minutes+"";
	            var minuteParts = minute.split("/");
	            var minutes = minuteParts[0].split("-");
	            var startMinutes = parseInt(minutes[0]);
	            var endMinutes = startMinutes;
	            if (minutes.length > 1) {
	                endMinutes = parseInt(minutes[1]);
	            }
	            
	            if (minuteParts.length > 1) {
	            	displaySchedule.interval = parseInt(minuteParts[1]);
	                displaySchedule.minuteSelected = "selected";
	                displaySchedule.hourSelected = "";
	                displaySchedule.daySelected = "";
	            }

	            var sHour = parseInt(hours[0]);
	            var sAMPM = "AM";
	            if (sHour > 12) {
	            	sAMPM = "PM";
	            	sHour -= 12;
	            }

	            var eHour = sHour;
	            if (hours.length > 1) {
	                eHour = parseInt(hours[1]);
	            }

	            eHour = Math.max(sHour, eHour);
	            var eAMPM = "AM";
	            if (eHour > 12) {
	            	eAMPM = "PM";
	            	eHour -= 12;
	            }
	            
	            displaySchedule.startTime = zeroPad(sHour, 2) + ":"+zeroPad(startMinutes, 2) + " "+sAMPM;
	            displaySchedule.endTime = zeroPad(eHour, 2) + ":"+zeroPad(endMinutes, 2) + " "+eAMPM;
				
				var weekdays = schedule.daysOfWeek.split(',');
				weekdays.forEach(function(weekday) {
					switch (weekday) {
						case "1":
							displaySchedule.sunChecked = "checked";
							break;
						case "2":
							displaySchedule.monChecked = "checked";
							break;
						case "3":
							displaySchedule.tueChecked = "checked";
							break;
						case "4":
							displaySchedule.wedChecked = "checked";
							break;
						case "5":
							displaySchedule.thuChecked = "checked";
							break;
						case "6":
							displaySchedule.friChecked = "checked";
							break;
						case "7":
							displaySchedule.satChecked = "checked";
							break;
					}
				});
			} else {
				displaySchedule.minuteSelected = "selected";
				displaySchedule.monChecked = "checked";
				displaySchedule.tueChecked = "checked";
				displaySchedule.wedChecked = "checked";
				displaySchedule.thuChecked = "checked";
				displaySchedule.friChecked = "checked";
			}
			return displaySchedule;
		},
		updateSchedules: function(schedule) {
			var schedules = this.schedules;
			var saveSchedules = {
				Schedule: [schedule],
				Retry: {
					Schedule: [],
					maxRetry: schedules.Retry.maxRetry
				},
				atomId: schedules.atomId,
				id: schedules.id,
				processId: schedules.processId
			}
			return saveSchedules;
		},
		selectConnection: function(index) {
			var connection = this.connections[index];
			$("#setupconnectionFields").append(_.template( conntemplate ) ( {id: connection.id, connections: connection.field} ));
			connection.field.forEach(function(field) {
				$("#cb-"+connection.id+"-"+field.id).click(function() {
					if ($("#"+connection.id+"-"+field.id).attr("disabled") === "disabled") {
						$("#"+connection.id+"-"+field.id).removeAttr("disabled");
					} else {
						$("#"+connection.id+"-"+field.id).attr("disabled", "disabled");
					}
					$("#"+connection.id+"-"+field.id).toggleClass("ui-state-disabled");
				});
			});
		},
		selectProcessProperty: function(index) {
			var processProperty = this.processProperties[index];
			$("#setupprocessPropFields").append(_.template( pptemplate ) ( {properties: processProperty.ProcessPropertyValue} ));
			processProperty.ProcessPropertyValue.forEach(function(field) {
				$("#cb-"+field.key).click(function() {
					if ($("#"+field.key).attr("disabled") === "disabled") {
						$("#"+field.key).removeAttr("disabled");
					} else {
						$("#"+field.key).attr("disabled", "disabled");
					}
					$("#"+field.key).toggleClass("ui-state-disabled");
				});
			});
		},
		selectMapExtension: function(index) {
			var mapExt = this.mapExtensions[index];
			if (this.browseDialogs[mapExt.id]) {
				this.openBrowseDialog(this.browseDialogs[mapExt.id].html, 
									  this.browseDialogs[mapExt.id].height,
									  mapExt);
			} else {
				this.apiUtils.getMapExtension({account: this.account, mapExtensionId: mapExt.id},
					function(mapExtension) {
						this.loadMapExtension(mapExtension);
					}.bind(this),
					function(error) {
						MessageDialogUtil.showMessageDialog("Error", "Failed to get MapExtension : "+error.message);
					}
				);
			}	
		},
		loadMapExtension: function(mapExtension) {
			this.mapExtension = mapExtension;
			$('#srctree').jstree(true).settings.core.data = xpathToJS(mapExtension.Map.SourceProfile.Node, mapExtension.Map.SourceProfileExtensions.Node);
			$('#srctree').jstree(true).refresh();
		
			$('#desttree').jstree(true).settings.core.data = xpathToJS(mapExtension.Map.DestinationProfile.Node, mapExtension.Map.DestinationProfileExtensions.Node);
			$('#desttree').jstree(true).refresh();
		
			$('#mappingsTable').DataTable().clear().draw();
			if (mapExtension.Map.ExtendedMappings && mapExtension.Map.ExtendedMappings.Mapping) {
				mapExtension.Map.ExtendedMappings.Mapping.forEach(function(mapping) {
					var mappingRow;
					if (mapping.toFunction) {
						mappingRow = {
							from: mapping.fromXPath.substring(1).replace(/\//g, " -> "),
							mapping: mapping
						};
						mapExtension.Map.ExtendedFunctions.Function.forEach(function(fn){
							if (fn.id === mapping.toFunction) {
								mappingRow.to = fn.id;
							}
						});
					} else if (mapping.fromFunction) {
						mappingRow = {
							to: mapping.toXPath.substring(1).replace(/\//g, " -> "),
							mapping: mapping
						};
						mapExtension.Map.ExtendedFunctions.Function.forEach(function(fn){
							if (fn.id === mapping.fromFunction) {
								mappingRow.from = fn.id;
							}
						});
					
					} else {
						mappingRow = {
							from: mapping.fromXPath.substring(1).replace(/\//g, " -> "),
							to: mapping.toXPath.substring(1).replace(/\//g, " -> "),
							mapping: mapping
						};
					}
					$('#mappingsTable').DataTable().row.add(mappingRow).draw(false);
				});
			}
			$('#srcDestExtTable').DataTable().clear().draw();
			if (mapExtension.Map.SourceProfileExtensions && mapExtension.Map.SourceProfileExtensions.Node) {
				mapExtension.Map.SourceProfileExtensions.Node.forEach(function(ext) {
					$('#srcDestExtTable').DataTable().row.add({name: ext.name, type: "Source", ext: ext}).draw(false);
				});
			}
			if (mapExtension.Map.DestinationProfileExtensions && mapExtension.Map.DestinationProfileExtensions.Node) {
				mapExtension.Map.DestinationProfileExtensions.Node.forEach(function(ext) {
					$('#srcDestExtTable').DataTable().row.add({name: ext.name, type: "Destination", ext: ext}).draw(false);
				});
			}
			/*
			$('#functionsTable').DataTable().clear().draw();
			if (mapExtension.Map.ExtendedFunctions && mapExtension.Map.ExtendedFunctions.Function) {
				mapExtension.Map.ExtendedFunctions.Function.forEach(function(fn) {
					$('#functionsTable').DataTable().row.add(fn).draw(false);
				});
			}
			*/
		},
		isValidMap: function() {
			var enable = false;
			if (this.selectedSrcNode && this.selectedDestNode) {
				enable = true;
				if (this.selectedSrcNode.children.length > 0) {
					enable = false;
				}
				if (this.selectedDestNode.children.length > 0) {
					enable = false;
				}
				if (enable && this.mapExtension.Map.ExtendedMappings && this.mapExtension.Map.ExtendedMappings.Mapping) {
					this.mapExtension.Map.ExtendedMappings.Mapping.forEach(function(mapping) {
						if (mapping.fromXPath === this.selectedSrcNode.id) {
							enable = false;
						}
						if (mapping.toXPath == this.selectedDestNode.id) {
							enable = false;
						}
					}.bind(this));
				}
			}
			if (enable) {
				$( "#map" ).button( "enable" );
			} else {
				$( "#map" ).button( "disable" );
			}
		},
		addMapping: function(from, to) {
			var mappingRow = {
				from: from.substring(1).replace(/\//g, " -> "),
				to: to.substring(1).replace(/\//g, " -> ")
			}
			var add = true;
			if (this.mapExtension.Map.ExtendedMappings && this.mapExtension.Map.ExtendedMappings.Mapping) {
				this.mapExtension.Map.ExtendedMappings.Mapping.forEach(function(mapping) {
					if (mapping.fromXPath === from) {
						add = false;
					}
					if (mapping.toXPath == to) {
						add = false;
					}
				});
			}
			if (add) {
				if (!this.mapExtension.Map.ExtendedMappings) {
					this.mapExtension.Map.ExtendedMappings = {
						"@type": "MapExtensionsExtendedMappings",
						"Mapping": []	
					};
				}
				if (!this.mapExtension.Map.ExtendedMappings.Mapping) {
					this.mapExtension.Map.ExtendedMappings.Mapping = [];
				}
				var mapping = {
					"@type": "MapExtensionsMapping",
					"toXPath": to,
                    "fromXPath": from
				};
				this.mapExtension.Map.ExtendedMappings.Mapping.push(mapping);
				mappingRow.mapping = mapping;
				$('#mappingsTable').DataTable().row.add(mappingRow).draw(false);
			}
		},
		setExtensionValues: function(ext) {
			if (!ext) {
				ext = {
					name: "",
					mandatory: false,
					enforceUnique: false,
					fieldLengthValidation: false,
					minLength: 0,
					maxLength: 0,
					Character: {}
				}
				$("#extName").prop("readonly", false);
			} else {
				$("#extName").prop("readonly", true);
			}
			$("#extName").val(ext.name),
			$('#extMandatory').prop('checked', ext.mandatory);
			$('#extEnforceUnique').prop('checked', ext.enforceUnique);
			$('#extFieldLengthValidation').prop('checked', ext.fieldLengthValidation);
			$('#extMinLen').spinner({step: 1, max: 100, min: 0});
			$('#extMaxLen').spinner({step: 1, max: 100, min: 0});
			$('#extMinLen').spinner("value", ext.minLength ? parseInt(ext.minLength) : 0);
			$('#extMaxLen').spinner("value", ext.maxLength ? parseInt(ext.maxLength) : 0);
			if (ext.Character) {
				$('#extDataType').val("Character");
				$('#extDataType').selectmenu("refresh");
			} else if (ext.Number) {
				$('#extDataType').val("Number");
				$('#extDataType').selectmenu("refresh");
				$("#dataFormatOptions").append(_.template( numberTemplate ) ( {} ));
				$('#numberFormat').val(ext.Number.format);
				$('#numberFormatSigned').prop('checked', ext.Number.signed);
				$('#numberFormatImpliedDecimals').val(parseInt(ext.Number.impliedDecimal));
				$('#numberFormatImpliedDecimals').spinner({step: 1, max: 100, min: 0});
			} else if (ext.DateTime) {
				$('#extDataType').val("Date/Time");
				$('#extDataType').selectmenu("refresh");
				$("#dataFormatOptions").append(_.template( dateTimeTemplate ) ( {} ));
				$('#dateFormat').val(ext.DateTime.format);
				$("#dateFormat").selectmenu({width: 150});
			}
		},
		saveMapExtension: function() {
			var mapExtension = this.mapExtension;
			var saveMapExtension = {
				environmentId: mapExtension.environmentId,
				extensionGroupId: mapExtension.extensionGroupId,
				id: mapExtension.id,
				mapId: mapExtension.mapId,
				name: mapExtension.name,
				processId: mapExtension.processId,
				Map: {}
			};
			if (mapExtension.Map.ExtendedFunctions) {
				saveMapExtension.Map.ExtendedFunctions = JSON.parse(JSON.stringify(mapExtension.Map.ExtendedFunctions));
			}
			if (mapExtension.Map.ExtendedMappings) {
				saveMapExtension.Map.ExtendedMappings = JSON.parse(JSON.stringify(mapExtension.Map.ExtendedMappings));
			}
			if (mapExtension.Map.SourceProfileExtensions) {
				saveMapExtension.Map.SourceProfileExtensions = JSON.parse(JSON.stringify(mapExtension.Map.SourceProfileExtensions));
			}
			if (mapExtension.Map.DestinationProfileExtensions) {
				saveMapExtension.Map.DestinationProfileExtensions = JSON.parse(JSON.stringify(mapExtension.Map.DestinationProfileExtensions));
			}
			if (mapExtension.Map.BrowseSettings) {
				saveMapExtension.Map.BrowseSettings = JSON.parse(JSON.stringify(mapExtension.Map.BrowseSettings));
			}
			
			function traverse(obj) {
				var p;
				for (p in obj) {
					if (typeof(obj[p])=="object") {
						traverse(obj[p]);
					} else if (p === "@type") {
						delete obj[p];
					}
				}
			}
			traverse(saveMapExtension);
			console.log(saveMapExtension);
			this.apiUtils.saveMapExtension({account: this.account, environmentMapExtension: saveMapExtension},
				function() {
					MessageDialogUtil.showMessageDialog("Save Complete", "Map Extension Save is complete");
				},
				function(error) {
					MessageDialogUtil.showMessageDialog("Error", "Map Extension Save failed : "+error.message);
				}
			);
		},
		createBrowseDialog: function(mapExtension) {
			var id = "browseDialog_"+mapExtension.id;
			var browseDialogStr = '<div id="browseDialog"'+id+'>';
			var fieldCount = 0;
			if (mapExtension.SourceFieldSet) {
				fieldCount += mapExtension.SourceFieldSet.BrowseField.length;
				var connectionName;
				this.extensions.connections.connection.forEach(function(c) {
					if (c.id === mapExtension.SourceFieldSet.connectionId) {
						connectionName = c.name;
					}
				});
				browseDialogStr += "<p><b>Source Credentials for "+mapExtension.name+"</b></p>";
				mapExtension.SourceFieldSet.BrowseField.forEach(function(field) {
					browseDialogStr += _.template( browsefieldTemplate ) ( {fieldId: field.name, fieldName: field.name} );
				});
			}
			if (mapExtension.DestinationFieldSet) {
				fieldCount += mapExtension.DestinationFieldSet.BrowseField.length;
				var connectionName;
				this.extensions.connections.connection.forEach(function(c) {
					if (c.id === mapExtension.DestinationFieldSet.connectionId) {
						connectionName = c.name;
					}
				});
				browseDialogStr += "<p><b>Destination Credentials for "+mapExtension.name+"</b></p>";
				mapExtension.DestinationFieldSet.BrowseField.forEach(function(field) {
					browseDialogStr += _.template( browsefieldTemplate ) ( {fieldId: field.name, fieldName: field.name} );
				});
			}
			browseDialogStr += '</div>';
			var browseDialogHtml = $(browseDialogStr);
			var height = fieldCount * 35;
			return {height: height, html: browseDialogHtml};
		},
		openBrowseDialog: function(browseDialogHtml, height, mapExtension) {
			var id = mapExtension.id;
			var _this = this;
			browseDialogHtml.dialog({
				title: "Enter Browse Credentials",
				height: height + 150,
				width: 500,
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
						var srcBrowseFields = [];
						if (mapExtension.SourceFieldSet) {
							mapExtension.SourceFieldSet.BrowseField.forEach(function(field) {
								srcBrowseFields.push({name: field.name, value: $("#"+field.name).val()});
							});
						}
						var destBrowseFields = [];
						if (mapExtension.DestinationFieldSet) {
							mapExtension.DestinationFieldSet.BrowseField.forEach(function(field) {
								destBrowseFields.push({name: field.name, value: $("#"+field.name).val()});
							});
						}
						_this.apiUtils.getMapExtensionWithBrowse({
							account: _this.account, 
							mapExtensionId: id, 
							srcBrowseFields: srcBrowseFields, 
							destBrowseFields: destBrowseFields},
							function(mapExtension) {
								_this.loadMapExtension(mapExtension);
							}.bind(this),
							function(error) {
								MessageDialogUtil.showMessageDialog("Error", "Failed to get MapExtension : "+error.message);
							}
						);						
					},
					Cancel: function() {
						$( this ).dialog( "close" );
						$('#srctree').jstree(true).settings.core.data = [{id: "/root",text: "Root",children: []}];
						$('#srctree').jstree(true).refresh();
						$('#desttree').jstree(true).settings.core.data = [{id: "/root",text: "Root",children: []}];
						$('#desttree').jstree(true).refresh();
						$('#mappingsTable').DataTable().clear().draw();
						$('#srcDestExtTable').DataTable().clear().draw();
					}
				}
			});
		},
		destroy: function() {
			$('#srctree').jstree("destroy");
			$('#desttree').jstree("destroy");
			$('#mappingsTable').DataTable().destroy(true);
			$('#functionsTable').DataTable().destroy(true);
			$('#setupprocs').DataTable().destroy(true);
			$('#setupips').DataTable().destroy(true);
			$('#setupinstalledips').DataTable().destroy(true);
		},
		href: "setup",
		title: "Setup"
	});
	
	return View;
});
