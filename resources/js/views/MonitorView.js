// Copyright (c) 2015 Dell Boomi, Inc.

define([
		'jquery', 
		'backbone',
		'underscore',
		'text!templates/monitor.html',
		'../uiconfig',
		'datatables',
		'datatablesui/dataTables.jqueryui',
		'datatablesui/dataTables.select',
		'datatablesui/dataTables.buttons'
		], 
function($, Backbone, _, template, config){
	var View = Backbone.View.extend({
		initialize: function(options) {
			this.template = _.template( template ) ( {href: this.href} );
			this.apiUtils = options.apiUtils;
			this.credentials = options.credentials;
			this.account = options.account;
			this.environment = options.environment;
		},
		render: function(){
			$(this.el).html(this.template);
			$( "#tabs" ).on("tabsactivate",
				function( event, ui ) {
					if (ui.newPanel.selector === "#monitor") {
						if (!this.setup) {
							this.setupTables();
						}
						this.openWebSocket();
					} else if (this.ws) {
						this.ws.close();
					}
				}.bind(this)
			);
		},
		setupTables: function() {
			this.setup = true;
			$('#eventsTable').DataTable( {
				columns: [
		            { "data": "eventDate", "width": "25em" },
		            { "data": "processName" },
		            { "data": "eventLevel" },
		            { "data": "status" },
		            { "data": "executionId" },
		            { "data": "atomName" }
		        ],		
		        select: {
		            style: 'single'
		        },
		        dom: 'frtip',
		        searching: false,
		        info: false
		    } );
		},
		openWebSocket: function() {
			if (window.WebSocket) {
				this.ws = new WebSocket(config.wsUrl()+"status");
			} else if (window.MozWebSocket) {
				this.ws = new MozWebSocket(config.wsUrl()+"status");
			} else {
				alert("No WebSocket Support !!!");
			}
			
			this.ws.onopen = function(event) {
				var options = {
					subAccountId: this.account.accountId	
				};
				if (this.credentials.isDirect) {
					options.userid = this.credentials.userid;
					options.pwd = this.credentials.password;
					options.accountId = this.credentials.accountId;
					options.apiurl = this.credentials.baseUrl;
				}
				this.ws.send(JSON.stringify({creds: options}));
			}.bind(this);
		    this.ws.onmessage = function(event) {
		    	var results = JSON.parse(event.data);		    	
        		$('#eventsTable').DataTable().clear().draw();
        		results.forEach(function(event) {
					$('#eventsTable').DataTable().row.add(event).draw(false);
				});
      		}.bind(this);
      		this.ws.onerror = function (error) {
  				console.log('WebSocket Error ' + error);
  				this.ws.close();
			};
		},
		destroy: function() {
			$('#eventsTable').DataTable().destroy(true);
		},
		href: "monitor",
		title: "Monitor"
	});
	
	return View;
});
		