// Copyright (c) 2015 Dell Boomi, Inc.

define([
		'jquery', 
		'backbone',
		'underscore',
		'../util/MessageDialogUtil',
		'text!templates/report.html',
		'datatables',
		'datatablesui/dataTables.jqueryui',
		'datatablesui/dataTables.select',
		'datatablesui/dataTables.buttons'
		], 
function($, Backbone, _, MessageDialogUtil, template){
	var View = Backbone.View.extend({
		initialize: function(options) {
			this.template = _.template( template ) ( {href: this.href} );
			this.apiUtils = options.apiUtils;
			this.account = options.account;
			this.environment = options.environment;
		},
		render: function(){
			$(this.el).html(this.template);
			$( "#tabs" ).on("tabsactivate",
				function( event, ui ) {
					if (ui.newPanel.selector === "#report" && !this.processes) {
						this.setupTables();
						this.refresh();
					}
				}.bind(this)
			);
			$( "#tabs" ).on("ipuirefresh",
				function( event, ui ) {
					if (this.processes)
						this.refresh();
				}.bind(this)
			);			
		},
		setupTables: function() {
			var apiUtils = this.apiUtils;
			var account = this.account;
			$('#processTable').DataTable( {
				columns: [
		            { "data": "name", "width": "40em" },
		            { "data": "id" }
		        ],		
		        select: {
		            style: 'single'
		        },
		        buttons: [
		            {
		            	extend: 'selected',
		                text: 'Run Process',
		                action: function ( e, dt, node, conf ) {
		                    var rows = dt.rows( { selected: true } ).every( function () {
		                    	var process = this.data();
		                    	apiUtils.runProcess({account: account, process: process},
		                    		function() {
		                    			$("#tabs").tabs("option", "active", 3);
		                    		},
		                    		function(error) {
				        				MessageDialogUtil.showMessageDialog("Error", "Run Process failed : "+error.message);
		                    		}
		                    	);
		                    });
		                }
		            }
		        ],
		        dom: 'Bfrtip',
		        searching: false,
		        info: false,
		        pagingType: "simple"
		    } );
			
			$('#processTable tbody').on( 'click', 'tr', function () {
				var process = $('#processTable').DataTable().row(".selected").data();
				if (process) {
			        apiUtils.getProcessDetails({account: this.account, processId: process.id},
						function(results) {
			        		$('#executionTable').DataTable().clear().draw();
			        		results.result.forEach(function(execution){
			        			$('#executionTable').DataTable().row.add(execution).draw(false);
			        		});
						},
						function(error) {
				        	MessageDialogUtil.showMessageDialog("Error", "Get Process Details failed : "+error.message);
						}
			        );
				}
		    }.bind(this) );
			
			$('#executionTable').DataTable( {
				columns: [
		            { 
		            	"data": "executionTime", 
		            	"width": "30em" 
		            },
		            { 
		            	"data": "status",
			            "width": "15%"
		            },
		            { 
		            	"data": "inboundDocumentCount",
		            	"width": "5%"
		            },
		            { 
		            	"data": "outboundDocumentCount", 
			            "width": "5%"
		            },
		            { 
		            	"data": "message",
		            	"defaultContent": ""
		            }
		        ],		
		        select: {
		            style: 'single'
		        },
		        dom: 'frtip',
		        searching: false,
		        info: false,
		        pageLength: 5,
		        pagingType: "simple"
		    } );
			
		},
		refresh: function() {
			this.apiUtils.findProcesses({account: this.account},
				function(processes) {
					this.processes = processes;
	        		$('#processTable').DataTable().clear().draw();
	        		$('#executionTable').DataTable().clear().draw();

					processes.result.forEach(function(process){
						$('#processTable').DataTable().row.add(process).draw(false);
					});
				}.bind(this),
				function(error) {
					console.log("error: "+error);
				}
			);
		},
		destroy: function() {
			$('#executionTable').DataTable().destroy(true);
			$('#processTable').DataTable().destroy(true);
		},
		href: "report",
		title: "Report"
	});
	
	return View;
});
