// Copyright (c) 2015 Dell Boomi, Inc.

define([
		'jquery', 
		'backbone',
		'underscore',
		'text!templates/accounts.html',
		'../util/MessageDialogUtil',
		'../uiconfig',
		'jqueryui/accordion',
		'jqueryui/selectmenu',
		'jqueryui/dialog',
		'datatables',
		'datatablesui/dataTables.jqueryui',
		'datatablesui/dataTables.select',
		'datatablesui/dataTables.buttons'
		], 
function($, Backbone, _, template, MessageDialogUtil, config) {
	function pad(val) {
		val = val + "";
		if (val.length == 1) {
			val = "0" + val;
		}
		return val;
	}
	
	var View = Backbone.View.extend({
		initialize: function(options) {
			this.template = _.template( template ) ( {href: this.href} );
			this.apiUtils = options.apiUtils;
			this.credentials = options.credentials;
			this.accountGroups = options.accountGroups;
			this.clouds = options.clouds;
			this.accountGroupConfig = options.accountGroupConfig;
		},
		render: function(){
			$(this.el).html(this.template);
			$( "#tabs" ).on("tabsactivate",
				function( event, ui ) {
					if (ui.newPanel.selector === "#accounts") {
						this.openWebSocket();
					} else if (this.ws) {
						this.ws.close();
					}
				}.bind(this)
			);
			
			setTimeout(function() {
                $( "#pwprompt" ).dialog({
                    autoOpen: false,
                    height: 200,
                    width: 400,
                    modal: true,
                    buttons: {
                    	Ok: function() {
                    		$("#pwprompt").dialog( "close" );
                    		var pw = $("#tokenpw").val();
                    		var accountId = $("#accountId").text();
                    		this.generateToken(pw, accountId);
                    	}.bind(this),
                    	Cancel: function() {
                    		$("#pwprompt").dialog( "close" );
                    	}
                    }
                });			                	
                $( "#tokendialog" ).dialog({
                    autoOpen: false,
                    height: 200,
                    width: 700,
                    modal: true,
                    buttons: {
                    	Ok: function() {
                    		$("#tokendialog").dialog( "close" );
                    	}
                    }
                });			                	
                $( "#createAccountDialog" ).dialog({
                    autoOpen: false,
                    height: 450,
                    width: 350,
                    modal: true,
                    buttons: {
                    	Ok: function() {
                    		$("#createAccountDialog").dialog( "close" );
        					var accountProvision = {
								"name": $("#newAccountName").val(),
								"street": $("#street").val(),
								"city": $("#city").val(),
								"stateCode": $("#state").val(),
								"zipCode": $("#zipcode").val(),
								"countryCode": $("#countryCode").val(),
								"status": $("#status option:selected").attr("id"),
								"product": this.selectedAccountGroupConfig.productCodes
							};
							if (accountProvision.status === "active") {
								var d = new Date();
								d.setMonth(new Date().getMonth() + 3);
								accountProvision.expirationDate = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
							}
							this.ws.send(JSON.stringify({
								accountProvision: accountProvision, 
								environmentName: this.selectedAccountGroupConfig.environmentName,
								accountGroupId: this.selectedAccountGroup.id,
								cloudId: this.selectedAccountGroupConfig.cloudId,
								atomName: this.selectedAccountGroupConfig.atomName
							}));
                    	}.bind(this),
	                	Cancel: function() {
	                		$("#createAccountDialog").dialog( "close" );
	                	}
                    }
                });			                	
				
				$("#accountGroups").selectmenu({width: 200});
				
				if (this.accountGroups.length > 0) {
					var accountGroupOptions = [];
					this.accountGroups.forEach(function(accountGroup){
						accountGroupOptions.push("<option value='"+accountGroup.id+"'>"+accountGroup.name+"</option>");
					});
					$( "#accountGroups" ).append(accountGroupOptions.join("")).selectmenu();
					$( "#accountGroups" ).selectmenu("refresh");
				
					$( "#accountGroups" ).selectmenu({
						change: function( event, ui ) {
							this.loadAccounts(ui.item.value);
						}.bind(this)
					});
				}
				/*
				$("#clouds").selectmenu({width: 200});
				var cloudOptions = [];
				this.clouds.forEach(function(cloud){
					cloudOptions.push("<option value='"+cloud.id+"'>"+cloud.name+"</option>");
				});
				$( "#clouds" ).append(cloudOptions.join("")).selectmenu();
				$( "#clouds" ).selectmenu("refresh");
				*/
				
				$('#accountstable').DataTable( {
					columns: [
			            { "data": "name", "width": "40em" },
			            { "data": "accountId" }
			        ],		
			        buttons: [
			            {
			                text: 'Add Account',
			                action: function ( e, dt, node, conf ) {
			                	$("#createAccountDialog").dialog("open");
			                }
			            },
			            {
			            	extend: 'selected',
			                text: 'Generate URL',
			                action: function ( e, dt, node, conf ) {
			                	var row = dt.row( { selected: true } ).data();
			        			$("#accountName").empty();
			        			$("#accountName").append(row.name);
			        			$("#accountId").empty();
			        			$("#accountId").append(row.accountId);
			                	$("#pwprompt").dialog("open");
			                }.bind(this)
			            }
			        ],
			        select: {
			            style:    'single',
			        },
			        dom: 'Bfrtip',
			        pageLength: 4,
			        searching: false,
			        info: false
			    } );
			    
				if (this.accountGroups.length > 0) {
					this.loadAccounts(this.accountGroups[0].id);
				}
				
				$('#provisioningtable').DataTable( {
					columns: [
			            { "data": "name", "width": "40em" },
			            { "data": "id" },
			            { "data": "accountId" },
			            { "data": "status" }
			        ],		
			        select: {
			            style:    'single',
			        },
			        dom: 'frtip',
			        searching: false,
			        pageLength: 4,
			        info: false
			    } );
				
			}.bind(this), 500);
		},
		loadAccounts: function(accountGroupId) {
			this.selectedAccountGroupConfig = this.accountGroupConfig[accountGroupId];
			if (this.selectedAccountGroupConfig) {
				$("#envName").text(this.selectedAccountGroupConfig.environmentName);
				$("#atomName").text(this.selectedAccountGroupConfig.atomName);
				$("#cloudId").text(this.selectedAccountGroupConfig.cloudId);
				var productCodes = [];
				this.selectedAccountGroupConfig.productCodes.forEach(function(pc) {
					productCodes.push(pc.productCode);
				});
				$("#productCodes").text(productCodes.join(" "));
			}
			
			this.accountGroups.forEach(function(accountGroup) {
				if (accountGroup.id === accountGroupId) {
					this.selectedAccountGroup = accountGroup;
				}
			}.bind(this));
			this.loadAccountsTable(accountGroupId);
		},
		loadAccountsTable: function(accountGroupId) {
			$('#accountstable').DataTable().clear().draw();
    		this.apiUtils.findSubAccounts({accountGroup: {id: accountGroupId}},
		    function(results) {
				results.accounts.forEach(function(account) {
					$('#accountstable').DataTable().row.add(account).draw(false);
				});
			}.bind(this),
			function(error)	{
				MessageDialogUtil.showMessageDialog("Error", "Error finding Accounts : "+error.message);
			});
		},
		generateToken: function(tokenpw, accountId) {
			var data = {
				password: tokenpw,
				accountId: accountId 
			};
			var creds;
			if (this.apiUtils.getCredentials) {
				var credentials = this.apiUtils.getCredentials();
				data.partnerUserId = credentials.userid;
				data.partnerPassword = credentials.password;
				data.partnerAccountId = credentials.accountId;
				creds = btoa(credentials.userid+":"+credentials.password);
			} else {
				creds = btoa(this.credentials.userid+":"+this.credentials.password)
			}
            $.ajax({
                url: "./crypto",
                type: "POST",
                data: JSON.stringify(data),
                contentType : "application/json",
                headers: {
                    "Authorization": "Basic " + creds,
                    "Accept": "application/json"
                },
                success: function(results, textStatus, jqXHR) {
        			$("#gendtoken").empty();
        			var url = window.location.protocol + "//" + window.location.host + "/#main/" + results.token;
        			$("#gendtoken").append(url);
                	$("#tokendialog").dialog("open");
                }.bind(this),
                error: function(jqXHR, textStatus, errorThrown) {
					MessageDialogUtil.showMessageDialog("Error", "Error generating token : "+errorThrown);
                } 
            });
		},
		openWebSocket: function() {
			if (window.WebSocket) {
				this.ws = new WebSocket(config.wsUrl()+"status");
			} else if (window.MozWebSocket) {
				this.ws = new MozWebSocket(config.wsUrl()+"status");
			} else {
				MessageDialogUtil.showMessageDialog("Error", "No WebSocket Support !!!");
			}
			
			this.ws.onopen = function(event) {
			}.bind(this);
		    this.ws.onmessage = function(event) {
		    	var results = JSON.parse(event.data);		    	
        		$('#provisioningtable').DataTable().clear().draw();
        		results.forEach(function(provision) {
        			if (provision.error && provision.error.responseText) {
        				try {
	        				provision.status = "FAILED: "+JSON.parse(provision.error.responseText).message;
	        			} catch(e) {
	        				provision.status = "FAILED: "+provision.error.responseText;
	        			}
        			}
        			if (provision.status === "READY") {
						this.loadAccountsTable(this.selectedAccountGroup.id);
        			} else {
        				if (!provision.id) {
        					provision.id = "";
        				}
        				if (!provision.accountId) {
        					provision.accountId = "";
        				}
						$('#provisioningtable').DataTable().row.add(provision).draw(false);
					}
				}.bind(this));
      		}.bind(this);
      		this.ws.onerror = function (error) {
  				console.log('WebSocket Error ' + error);
  				this.ws.close();
			};
		},
		href: "accounts",
		title: "Accounts"
	});
	
	return View;
});
