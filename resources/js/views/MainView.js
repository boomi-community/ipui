// Copyright (c) 2015 Dell Boomi, Inc.

define([
		'jquery', 
		'backbone',
		'underscore',
		'text!templates/main.html',
		'text!templates/creds.html',
		//'APIUtils',
		'../util/APIUtilsProxy',
		'./ReportView',
		'./MonitorView',
		'./SetupView',
		'../util/Cookie',
		'../uiconfig',
		'../util/MessageDialogUtil',
		'../models/IntegrationPackList',
		'../models/IntegrationPackInstanceList',
		'../models/IntegrationPackInstance',
		'jqueryui/tabs',
		'jqueryui/dialog',
		'jqueryui/button',
		'jqueryui/menu',
		'jqueryui/progressbar'
		], 
function(
	$, 
	Backbone, 
	_, 
	template, 
	creds, 
	//APIUtils, 
	APIUtilsProxy, 
	ReportView, 
	MonitorView, 
	SetupView, 
	CookieUtil, 
	config, 
	MessageDialogUtil, 
	IntegrationPackList, 
	IntegrationPackInstanceList, 
	IntegrationPackInstance
) {
	if (!Array.isArray) {
		Array.isArray = function(arg) {
			return Object.prototype.toString.call(arg) === '[object Array]';
		};
	}
	
	var View = Backbone.View.extend({
		initialize: function(options) {
			var selfService = false;
			if (options.token) {
				this.token = options.token;
			} else if (options.provisionId) {
				this.provisionId = options.provisionId;
				selfService = true;
			} else {
				selfService = true;
			}
    		this.password = CookieUtil.read(config.TOKEN_SECRET_COOKIE);
			this.template = _.template( template ) ( {loggedIn: this.password == '' ? false : true, selfService: selfService} );
			this.creds = _.template( creds ) ( {} );
			this.tabWidgets = [];
		},
		render: function(){
			$(this.el).html(this.template);

			this.setupLoginButton();
			this.$("#logout").button().click(function() {
				this.logoff();
			}.bind(this));
			this.$("#selfService").button().click(function() {
			    $("#createAccountDialog").dialog("open");
			}.bind(this));

			this.$("#tabs").tabs();
			if (this.password !== '') {
				this.login();
			}
			
			$(document).keyup(function(e) {
				if (e.keyCode == 13) {
					var activeTab = $("#tabs").tabs( "option", "active" );
					if (activeTab === 0) {
						this.password = $("#password").val();
						if (!this.token) {
							$("#createAccountDialog").dialog("open");
						} else if (this.password) {
	    					CookieUtil.write(config.TOKEN_SECRET_COOKIE, this.password);
							this.login();
						} else {
							this.logoff();
						}
					}
				}
			}.bind(this));
			
			setTimeout(function() {
                $("#createAccountDialog").dialog({
                    autoOpen: false,
                    height: 475,
                    width: 350,
                    modal: true,
                    buttons: {
                    	Ok: function() {
                    		var accountName = $("#newAccountName").val();
							var passphrase = $("#passphrase").val();
							if (accountName !== "" && passphrase != "") {
			                    $("#createAccountDialog").dialog("close");
								var accountProvision = {
									"name": accountName,
									"street": $("#street").val(),
									"city": $("#city").val(),
									"stateCode": $("#state").val(),
									"zipCode": $("#zipcode").val(),
									"countryCode": $("#countryCode").val(),
									"status": "trial"
								};
								var instance = $("#progressDialog").dialog("instance");
								instance.provisionRequest = {
									accountProvision: accountProvision,
									passphrase: passphrase,
									selfService: true,
									installDirectory: $("#installDirectory").val(),
									localAtom: $('input[id="localAtom"]:checked').val() !== undefined
								};
			                    $("#progressDialog").dialog("open");
								$("#progressbar").progressbar( "value", 1);
							} else {
								$("#errmsg").text("Account Name and Passphrase must be provided");
							}
                    	}.bind(this),
	                	Cancel: function() {
	                		$("#createAccountDialog").dialog("close");
	                	}
                    }
                });
                
                $('input[type=radio][name=atomType]').change(function() {
                	if (this.id === "localAtom") {
                		$("#createAccountDialog").dialog({height: 510});
                		$("#installDirContainer").css("display", "inline");
                	} else {
                		$("#createAccountDialog").dialog({height: 475});
                		$("#installDirContainer").css("display", "none");
                	}
                });
                
                $("#progressDialog").dialog({
                    autoOpen: false,
        			closeOnEscape: false,
        			resizable: false,                    
                    modal: true,
        			open: function() {
        				var provisionRequest = $("#progressDialog").dialog("instance").provisionRequest;
						this.openWebSocket(
							function() {
								this.ws.send(JSON.stringify(provisionRequest));
							}.bind(this),
							function(msg) {
								if (Array.isArray(msg)) {
									msg.forEach(function(provision) {
										if (provision.name === provisionRequest.accountProvision.name) {
											switch (provision.status) {
												case "PENDING":
													this.provisionId = provision.id;
													var href = window.location.href;
													if (href.indexOf("#provision") == -1) {
														window.history.replaceState({} , 'Acme Integrations', href+"#provision/"+provision.id);
													}
													$("#progressbar").progressbar( "value", 10);
													break;
												case "COMPLETED":
													$("#progressbar").progressbar( "value", 50);
													break;
												case "ENV_CREATED":
													$("#progressDialog").dialog( "option", "buttons", []);
													$("#progressbar").progressbar( "value", 70);
													break;
												case "ATTACHED_TO_ACCOUNT_GROUP":
													$("#progressbar").progressbar( "value", 80);
													break;
												case "ATOM_CREATED":
													$("#progressbar").progressbar( "value", 90);
													break;
												case "READY":
													this.token = provision.token;
													this.password = provisionRequest.passphrase;
													var href = window.location.href;
													if (href.indexOf("#") != -1) {
														href = href.substring(0, href.indexOf("#"));
													}
													window.history.replaceState({} , 'Acme Integrations', href+"#"+this.token);
													CookieUtil.write(config.TOKEN_SECRET_COOKIE, this.password);
													$("#progressbar").progressbar( "value", 100);
													break;
												case "ACCOUNT_PROVISIONED":
													$("#progressbar").progressbar( "value", 50);
													this.provisionedAccountId = provision.accountId;
													$("#progressLabel").text( "Account Created. Use the button below to get an Atom Installer. Download it and run 'java -jar AtomInstaller.jar' on your selected machine" );
													$("#progressDialog").dialog( "option", "buttons", [{
														text: "Download Atom Installer",
														click: function() {
															var form = $('<form>', {
																'action': './atominstall/'+this.provisionedAccountId,
																'target': '_blank',
															});
															$(document.body).append(form);
															form.submit();	
														}.bind(this)
													}]);
													break;
												case "FAILED":
													$("#progressbar").progressbar( "value", 100);
						        					$("#progressLabel").text( provision.error );
													break;
											}
										}
									}.bind(this));
								} else {
									provisionRequest.accountProvision = msg;
									console.log(msg);
									$("#progressDialog").dialog("instance").provisionRequest = provisionRequest;
								}	
							}.bind(this)
						);
        			}.bind(this)
                });
                
    			$("#progressbar").progressbar({
    				value: false,
					change: function() {
						$("#progressLabel").text("Creating Account: " + $("#progressbar").progressbar( "value" ) + "%" );
					},
					complete: function() {
          				if (this.token) {
        					$("#progressLabel").text( "Account Created" );
        				}
        				$("#progressDialog").dialog( "option", "buttons", [{
				        	text: "Close",
          					click: function() {
          						$("#progressDialog").dialog("close");
          						if (this.token) {
			        				$("#selfServiceForm").remove();
	          						this.login();
	          					}
          					}.bind(this)
				        }]);
				        $(".ui-dialog button").last().focus();					
      				}.bind(this)
    			});                
    			
    			if (this.provisionId) {
					var instance = $("#progressDialog").dialog("instance");
					instance.provisionRequest = {
						accountProvision: {
							id: this.provisionId,
							name: ""
						}	
					};
    			
					$("#progressDialog").dialog("open");
					$("#progressbar").progressbar( "value", 10);
    			}    
                		                	
			}.bind(this), 500);
			
		},
		login: function() {
            $.ajax({
                url: "./crypto",
                type: "POST",
                data: JSON.stringify({token: this.token, password: this.password}),
                contentType : "application/json",
                headers: {
                    "Accept": "application/json"
                },
                success: function(credentials, textStatus, jqXHR) {
                	this.startSessionMonitor();
        			config.setBaseUrl(credentials.baseUrl);
        			var options = {
        				userid: credentials.partnerUserId,
    					password: credentials.partnerPassword,
    					accountId: 	credentials.partnerAccountId,
    					baseUrl: credentials.baseUrl,
    					isDirect: credentials.isDirect,
    					debug: credentials.debug
        			}
        			var apiUtils;
        			if (credentials.isDirect) {
        				apiUtils = APIUtils.create(options);
        			} else {
        				apiUtils = APIUtilsProxy.create({token: this.token, password: this.password});
        			}
        			
        			apiUtils.getAccount({accountId: credentials.accountId}, 
        			function(results) {
        				if (results) {
        					this.account = results.account;
        					this.environment = results.environment;
        					this.available = new IntegrationPackList(results.available);
        					this.installed = new IntegrationPackInstanceList(results.installed);
        					
        					this.addTab(new SetupView({apiUtils: apiUtils, account: this.account, environment: this.environment, available: this.available, installed: this.installed}));
        					this.addTab(new ReportView({apiUtils: apiUtils, account: this.account, environment: this.environment}));
        					this.addTab(new MonitorView({apiUtils: apiUtils, credentials: options, account: this.account, environment: this.environment}));
        		        	if (this.installed.length > 0) {
        		        		$( "#tabs" ).tabs( "enable", 2 );	
        		        		$( "#tabs" ).tabs( "enable", 3 );	
        		        	} else {
        		        		$( "#tabs" ).tabs( "disable", 2 );	
        		        		$( "#tabs" ).tabs( "disable", 3 );	
        		        	}
        				} else {
    	        			MessageDialogUtil.showMessageDialog("Error", "Error finding Account");
        				}
        				$("#creds").remove();
        				$("#selfServiceForm").remove();
        				$("#welcome").append("<button id=\"logout\">Log off</button>");
        				$("#logout").button().click(function() {
        					this.logoff();
        				}.bind(this));
        			}.bind(this),
        			function(error)	{
	        			MessageDialogUtil.showMessageDialog("Error", "Error finding Account : "+error.message);
        			});
                }.bind(this),
                error: function(jqXHR, textStatus, errorThrown) {
        			MessageDialogUtil.showMessageDialog("Error", "Error validating token : "+errorThrown);
					CookieUtil.write(config.TOKEN_SECRET_COOKIE, '');
                }.bind(this)
            });
		},
		logoff: function() {
			this.stopSessionMonitor();
			this.tabWidgets.forEach(function(tabWidget) {
				if (tabWidget.destroy) {
					tabWidget.destroy();
				}
			});
			this.tabs = [];
			CookieUtil.write(config.TOKEN_SECRET_COOKIE, '');
			$('[data-key="setup"]').remove();			
			$('[data-key="report"]').remove();
			$('[data-key="monitor"]').remove();
			$("#tabs").tabs( "refresh" );	

			$("#logout").remove();
			$("#welcome").append(this.creds);
			this.setupLoginButton();
		},
		setupLoginButton: function() {
			this.$("#login").button().click(function() {
				this.password = $("#password").val();
    			CookieUtil.write(config.TOKEN_SECRET_COOKIE, this.password);
				this.login();
			}.bind(this));
		},
		addTab: function(tab) {
			this.tabWidgets.push(tab);
			var liTemplate = "<li data-key='#{key}'><a href='#{href}'>#{label}</a></li>";
			$("#tabs").find(".ui-tabs-nav").append($(liTemplate.replace( /#\{href\}/g, "#" + tab.href ).replace( /#\{label\}/g, tab.title).replace( /#\{key\}/g, tab.href)));
			tab.render();
			$("#tabs").append($(tab.el));
			$("#tabs").tabs( "refresh" );			
		},
		startSessionMonitor: function() {
			this.intervalId = setInterval(function() {
				var tokenSecret = CookieUtil.read(config.TOKEN_SECRET_COOKIE);
				if (tokenSecret == '') {
					this.logoff();
				}
			}.bind(this), 30000);
		},
		stopSessionMonitor: function() {
			if (this.intervalId) {
				clearInterval(this.intervalId);
				this.intervalId = undefined;
			}
		},
		openWebSocket: function(connCb, msgCb) {
			if (window.WebSocket) {
				this.ws = new WebSocket(config.wsUrl()+"status");
			} else if (window.MozWebSocket) {
				this.ws = new MozWebSocket(config.wsUrl()+"status");
			} else {
				alert("No WebSocket Support !!!");
			}
			
			this.ws.onopen = function(event) {
				connCb();
			}.bind(this);
		    this.ws.onmessage = function(event) {
		    	var results = JSON.parse(event.data);
		    	msgCb(results);	    	
      		}.bind(this);
      		this.ws.onerror = function (error) {
  				console.log('WebSocket Error ' + error);
  				this.ws.close();
			};
		}
	});
	
	return View;
});
