// Copyright (c) 2015 Dell Boomi, Inc.

define([
		'jquery', 
		'backbone',
		'underscore',
		'text!templates/admin.html',
		'text!templates/admincreds.html',
		//'APIUtils',
		'../util/APIUtilsProxy',
		'../util/Cookie',
		'../uiconfig',
		'../util/MessageDialogUtil',
		'../views/AccountsView',
		'jqueryui/tabs',
		'jqueryui/dialog',
		'jqueryui/button',
		'jqueryui/menu'
		], 
function(
	$, 
	Backbone, 
	_, 
	template, 
	creds,
	//APIUtils, 
	APIUtilsProxy, 
	CookieUtil, 
	config, 
	MessageDialogUtil, 
	AccountsView
) {
	var View = Backbone.View.extend({
		initialize: function(options) {
			this.credentials = {};
			this.credentials.userid = CookieUtil.read(config.TOKEN_ADMIN_ID_COOKIE); 
    		this.credentials.password = CookieUtil.read(config.TOKEN_ADMIN_PWD_COOKIE);
    		this.credentials.accountId = CookieUtil.read(config.TOKEN_ACCOUNT_COOKIE);
			this.template = _.template( template ) ( {loggedIn: this.credentials.userid == '' ? false : true} );
			this.creds = _.template( creds ) ( {} );
		},
		render: function(){
			$(this.el).html(this.template);
				
			this.setupLoginButton();
			if (this.credentials.userid !== '') {
				this.login();
			}
			this.$("#tabs").tabs();
			
			$(document).keyup(function(e) {
				if (e.keyCode == 13) {
					var activeTab = $("#tabs").tabs( "option", "active" );
					if (activeTab === 0) {
						if 	(CookieUtil.read(config.TOKEN_ADMIN_PWD_COOKIE) === '') {
							this.loginButtonClick();
						} else {
							this.logoff();
						}
					}
				}
			}.bind(this));
		},
		login: function() {
            $.ajax({
                url: "./crypto",
                type: "GET",
                headers: {
                    "Authorization": "Basic " + btoa(this.credentials.userid+":"+this.credentials.password),
                    "Accept": "application/json"
                },
                success: function(results, textStatus, jqXHR) {
                	this.startSessionMonitor();
        			var apiUtils;
        			if (results.isDirect) {
        				apiUtils = APIUtils.create({
            				baseUrl: results.baseUrl,
            				userid: this.credentials.userid,
            				password: this.credentials.password,
            				accountId: this.credentials.accountId,
            				debug: results.debug
            			});
        			} else {
        				apiUtils = APIUtilsProxy.create({userid: this.credentials.userid, password: this.credentials.password});
        			}
        			var accountGroupConfig = results.accountGroupConfig;
					apiUtils.collectAdminData(undefined,
		        	function(results) {
						this.addTab(new AccountsView({
							apiUtils: apiUtils, 
							credentials: this.credentials, 
							accountGroups: results.accountGroups, 
							clouds: results.clouds,
							accountGroupConfig: accountGroupConfig
						}));
						$("#creds").remove();
						$("#admintab").append("<button id=\"logout\">Log off</button>");
						$("#logout").button().click(function() {
							this.logoff();
						}.bind(this));
					}.bind(this),
					function(error)	{
						MessageDialogUtil.showMessageDialog("Error", "Error finding Accounts : "+error.message);
					});
                }.bind(this),
                error: function(jqXHR, textStatus, errorThrown) {
        			MessageDialogUtil.showMessageDialog("Error", "Error validating token : "+errorThrown);
                } 
            });
		},
		logoff: function() {
			this.stopSessionMonitor();
			CookieUtil.write(config.TOKEN_ADMIN_ID_COOKIE, '');
			CookieUtil.write(config.TOKEN_ADMIN_PWD_COOKIE, '');
			CookieUtil.write(config.TOKEN_ACCOUNT_COOKIE, '');
			$('[data-key="accounts"]').remove();			
			$("#tabs").tabs( "refresh" );			
			$("#logout").remove();
			$("#admintab").append(this.creds);
			this.setupLoginButton();
		},
		setupLoginButton: function() {
			this.$("#login").button().click(function() {
				this.loginButtonClick();
			}.bind(this));
		},
		loginButtonClick: function() {
			this.credentials = {};
			this.credentials.userid = decodeURIComponent($("#id").val());
			this.credentials.password = $("#password").val();
			this.credentials.accountId = $("#accountid").val();
			CookieUtil.write(config.TOKEN_ADMIN_ID_COOKIE, this.credentials.userid);
			CookieUtil.write(config.TOKEN_ADMIN_PWD_COOKIE, this.credentials.password);
			CookieUtil.write(config.TOKEN_ACCOUNT_COOKIE, this.credentials.accountId);
			this.login();
		},
		addTab: function(tab) {
			var liTemplate = "<li data-key='#{key}'><a href='#{href}'>#{label}</a></li>";
			$("#tabs").find(".ui-tabs-nav").append($(liTemplate.replace( /#\{href\}/g, "#" + tab.href ).replace( /#\{label\}/g, tab.title).replace( /#\{key\}/g, tab.href)));
			tab.render();
			$("#tabs").append($(tab.el));
			$("#tabs").tabs( "refresh" );			
		},
		startSessionMonitor: function() {
			this.intervalId = setInterval(function() {
				var id = CookieUtil.read(config.TOKEN_ADMIN_ID_COOKIE); 
				var pwd = CookieUtil.read(config.TOKEN_ADMIN_PWD_COOKIE);
				if (id == '' && pwd == '') {
					this.logoff();
				}
			}.bind(this), 30000);
		},
		stopSessionMonitor: function() {
			if (this.intervalId) {
				clearInterval(this.intervalId);
				this.intervalId = undefined;
			}
		}
	});
	
	return View;
});
