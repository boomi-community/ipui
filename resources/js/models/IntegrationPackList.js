// Copyright (c) 2015 Dell Boomi, Inc.

define(['backbone', './IntegrationPack', '../uiconfig'], function(Backbone, IntegrationPack, config) {
	var IntegrationPackList = Backbone.Collection.extend({
		model: IntegrationPack
	});
	return IntegrationPackList;
});
