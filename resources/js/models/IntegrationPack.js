// Copyright (c) 2015 Dell Boomi, Inc.

define(['backbone'], function(Backbone) {
	var IntegrationPack = Backbone.Model.extend({
		idAttribute: "id"
	});
	
	return IntegrationPack;
});