// Copyright (c) 2015 Dell Boomi, Inc.

define(['backbone'], function(Backbone) {
	var IntegrationPackInstance = Backbone.Model.extend({
		idAttribute: "integrationPackId"
	});
	
	return IntegrationPackInstance;
});