// Copyright (c) 2015 Dell Boomi, Inc.

define(['backbone', './IntegrationPackInstance', '../uiconfig'], function(Backbone, IntegrationPackInstance, config) {
	var IntegrationPackInstanceList = Backbone.Collection.extend({
		model: IntegrationPackInstance
	});
	return IntegrationPackInstanceList;
});
