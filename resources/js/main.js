// Copyright (c) 2015 Dell Boomi, Inc.

require.config({
	baseUrl: 'js/',
	paths: {
		BoomiAPI: '../../BoomiAPI',
		APIUtils: '../../APIUtils',
		jquery: '../lib/jquery/jquery-2.1.4',
		jqueryui: '../lib/jquery-ui',
		underscore: '../lib/underscore/underscore-1.8.3',
		backbone: '../lib/backbone/backbone-1.2.0',
		q: '../lib/q/q',
		datatables: '../lib/datatables/jquery.dataTables',
		datatablesui: '../lib/datatables',
		jquerysteps: '../lib/jquery-steps/jquery.steps',
		jstree: '../lib/jstree/jstree',
		globalize: '../lib/globalize/globalize',
		cldr: '../lib/cldr/cldr',
		text: '../lib/requirejs/text',
		i18n: '../lib/requirejs/i18n',
		json: '../lib/requirejs/json',
		templates: '../templates',
		cldrdata: '../cldr-data'
	},
	shim: {
        'jquerysteps': { 
			deps: ['jquery']
		}	
	}
});

require(['app']);
