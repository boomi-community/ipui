// Copyright (c) 2015 Dell Boomi, Inc.
define([
	'jquery'
	],
function($) {
	return {
		showMessageDialog: function (title, content) {
			$("#dialog-message-content").empty();
			$("#dialog-message-content").append(content);
		    $("#dialog-message").dialog({
		    	title: title,
		    	modal: true,
		        buttons: {
		        	Ok: function() {
		        		$( this ).dialog( "close" );
		        	}
		        }
		    });
		}
	}
});