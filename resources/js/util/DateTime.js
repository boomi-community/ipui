// Copyright (c) 2015 Dell Boomi, Inc.

define(function() {
	function pad(val) {
		val = val + "";
		if (val.length == 1) {
			val = "0" + val;
		}
		return val;
	}
	
	return {
		format: function(dt) {
			var d = new Date(dt);
			var month = pad(d.getMonth()+1);
			var day = pad(d.getDate());
			var hour = d.getHours();
			var ampm;
			if (hour < 12) {
				ampm = "AM";
			} else {
				ampm = "PM";
			}
			if (hour == 0) {
				hour = 12;
			}
			if (hour > 12) {
				hour = hour - 12;
			}

			var mins = pad(d.getMinutes());
			var secs = pad(d.getSeconds());
			return d.getFullYear()+"-"+month+"-"+day+" "+hour+":"+mins+":"+secs+ " "+ampm;
		},
		formatTime: function(hour, minutes) {
			var ampm;
			if (hour < 12) {
				ampm = "AM";
			} else {
				ampm = "PM";
			}
			if (hour == 0) {
				hour = 12;
			}
			if (hour > 12) {
				hour = hour - 12;
			}

			var mins = pad(minutes);
			return hour+":"+mins+" "+ampm;
		},
		formatDuration: function(value) {
		    function twoDigitFormat(i, format) {
		        if (format && i < 10) {
		            return "0" + i;
		        }
		        return ""+i;
		    }
			
	        var duration = Math.round(value / 1000);
	        var seconds = Math.round(duration % 60);
	        var minutes = Math.round(duration / 60);
	        var hours = Math.round(minutes / 60);
	        minutes = Math.round(minutes % 60);
			var strDuration = "";
	        if (hours > 0) {
	        	strDuration += hours + " hr ";
	        }
	        if (seconds > 0 || minutes > 0 || hours > 0) {
	        	strDuration += twoDigitFormat(minutes, hours > 0) + " min ";
	        }
	        strDuration += twoDigitFormat(seconds, seconds > 0 || minutes > 0 || hours > 0);
	        strDuration += " sec";
	        return strDuration;
		}
	}
});