// Copyright (c) 2015 Dell Boomi, Inc.

define([
	'jquery'
], function($) {
	
	function callProxy(data, authHeaders, cb, failCb) {
		var headers = {
			"Accept": "application/json"
		};
		for (var headerName in authHeaders) {
			headers[headerName] = authHeaders[headerName];
		}
        $.ajax({
            url: "./api",
            type: "POST",
			dataType: "json",
            data: JSON.stringify(data),
            contentType : "application/json",
            headers: headers,
            success: function(results, textStatus, jqXHR) {
            	if (results.error) {
            		failCb(results.error);
            	} else {
	            	cb(results);
	            }
            },
            error: function(jqXHR, textStatus, errorThrown) {
            	failCb(errorThrown);
            } 
        });
	}
	
    var APIHandler = function(options) {
    	if (options.token) {
    		this.authHeaders = {
    			"X-Auth-Token": options.token,
    			"X-Auth-Secret": options.password 
    		};
    	} else if (options.userid) {
    		this.authHeaders = {
    			"Authorization": "Basic " + btoa(options.userid+":"+options.password)
    		};
    	}
    };
    
    APIHandler.prototype = {
    	getAccount: function(d, cb, failCb) {
			callProxy({data: d, apiName: "getAccount"}, this.authHeaders, cb, failCb);
    	},
		findAccount: function(d, cb, failCb) {
			callProxy({data: d, apiName: "findAccount"}, this.authHeaders, cb, failCb);
		},
		createAndRegisterAccount: function(d, cb, failCb) {
			callProxy({data: d, apiName: "createAndRegisterAccount"}, this.authHeaders, cb, failCb);
		},
		createIntegrationPackInstance: function(d, cb, failCb) {
			callProxy({data: d, apiName: "createIntegrationPackInstance"}, this.authHeaders, cb, failCb);
		},
		deleteIntegrationPackInstance: function(d, cb, failCb) {
			callProxy({data: d, apiName: "deleteIntegrationPackInstance"}, this.authHeaders, cb, failCb);
		},
		createAndAttachCloudAtom: function(d, cb, failCb) {
			callProxy({data: d, apiName: "createAndAttachCloudAtom"}, this.authHeaders, cb, failCb);
		},
		getEnvironmentExtensions: function(d, cb, failCb) {
			callProxy({data: d, apiName: "getEnvironmentExtensions"}, this.authHeaders, cb, failCb);
		},
		findProcesses: function(d, cb, failCb) {
			callProxy({data: d, apiName: "findProcesses"}, this.authHeaders, cb, failCb);
		},
		getProcessDetails: function(d, cb, failCb) {
			callProxy({data: d, apiName: "getProcessDetails"}, this.authHeaders, cb, failCb);
		},
		runProcess: function(d, cb, failCb) {
			callProxy({data: d, apiName: "runProcess"}, this.authHeaders, cb, failCb);
		},
		saveEnvironmentExtensions: function(d, cb, failCb) {
			callProxy({data: d, apiName: "saveEnvironmentExtensions"}, this.authHeaders, cb, failCb);
		},
		getProcessSchedules: function(d, cb, failCb) {
			callProxy({data: d, apiName: "getProcessSchedules"}, this.authHeaders, cb, failCb);
		},
		updateProcessSchedules: function(d, cb, failCb) {
			callProxy({data: d, apiName: "updateProcessSchedules"}, this.authHeaders, cb, failCb);
		},
		findSubAccounts: function(d, cb, failCb) {
			callProxy({data: d, apiName: "findSubAccounts"}, this.authHeaders, cb, failCb);
		},
		collectAdminData: function(d, cb, failCb) {
			callProxy({data: d, apiName: "collectAdminData"}, this.authHeaders, cb, failCb);
		},
		getMapExtension: function(d, cb, failCb) {
			callProxy({data: d, apiName: "getMapExtension"}, this.authHeaders, cb, failCb);
		},
		getMapExtensionWithBrowse: function(d, cb, failCb) {
			callProxy({data: d, apiName: "getMapExtensionWithBrowse"}, this.authHeaders, cb, failCb);
		},
		provisionAccount: function(d, cb, failCb) {
			callProxy({data: d, apiName: "provisionAccount"}, this.authHeaders, cb, failCb);
		},
		getProvisionAccountStatus: function(d, cb, failCb) {
			callProxy({data: d, apiName: "getProvisionAccountStatus"}, this.authHeaders, cb, failCb);
		},
		saveMapExtension: function(d, cb, failCb) {
			callProxy({data: d, apiName: "saveMapExtension"}, this.authHeaders, cb, failCb);
		}
	};

	return {
		create: function(options) {
			return new APIHandler(options);
		}
	};
});