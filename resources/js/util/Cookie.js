// Copyright (c) 2014 Dell Boomi, Inc.
define(function() {
	function writeCookie(name, value, time) {
	    var date = new Date();
	    date.setTime(date.getTime() + time);
	    document.cookie = name + "=" + value + "; expires=" + date.toGMTString() + "; path=/";
	}
	
	function readCookie(name) {
	    var i, c, ca, nameEQ = name + "=";
	    ca = document.cookie.split(';');
	    for(i=0;i < ca.length;i++) {
	        c = ca[i];
	        while (c.charAt(0)==' ') {
	            c = c.substring(1,c.length);
	        }
	        if (c.indexOf(nameEQ) == 0) {
	            return c.substring(nameEQ.length,c.length);
	        }
	    }
	    return '';
	}
	return {
		write: function(name, value) {
			writeCookie(name, value, (15 * 60 * 1000));
		},
		read: function(name) {
			return readCookie(name);
		}
	}
});