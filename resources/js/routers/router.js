// Copyright (c) 2015 Dell Boomi, Inc.

define([
	'jquery', 
	'backbone', 
	'underscore',
	'views/MainView',
	'../util/Cookie',
	'../uiconfig'
	], 
function(
	$, 
	Backbone, 
	_,
	MainView,
	CookieUtil,
	config
) {
	var Router = Backbone.Router.extend({
		initialize: function(options) {
			$.ajaxSetup({
				beforeSend: function(xhr, settings) {
					var tokenSecret = CookieUtil.read(config.TOKEN_SECRET_COOKIE);
					if (tokenSecret !== '') {
						CookieUtil.write(config.TOKEN_SECRET_COOKIE, tokenSecret);
					}
					return true;
				}.bind(this)
			});
			$( document ).ajaxStart(function() {
				$( "#spinner" ).show();
			});			
			$( document ).ajaxStop(function() {
				$( "#spinner" ).hide();
			});			
			$('.back').on('click', function(event) {
	            window.history.back();
	            return false;
	        });
	        this.on("route:main", function(token) {
	        	this.page = new MainView({token: token});
	        	this.page.render();
		        $('body').append($(this.page.el));
	        });
	        this.on("route:provision", function(provisionId) {
	        	this.page = new MainView({provisionId: provisionId});
	        	this.page.render();
		        $('body').append($(this.page.el));
	        });
			Backbone.history.start();
		},	
		routes: {
			'main/:token': 'main',
			'provision/:provisionId': 'provision',
			'/main/provision/:provisionId': 'provision',
			':token': 'main',
			'': 'main'
		}
		
	});
	
	return Router;
});
