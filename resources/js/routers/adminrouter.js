// Copyright (c) 2015 Dell Boomi, Inc.

define([
	'jquery', 
	'backbone', 
	'underscore',
	'views/AdminView',
	'../util/Cookie',
	'../uiconfig'
	], 
function(
	$, 
	Backbone, 
	_,
	AdminView,
	CookieUtil,
	config
) {
	var Router = Backbone.Router.extend({
		initialize: function(options) {
			$.ajaxSetup({
				beforeSend: function(xhr, settings) {
					var id = CookieUtil.read(config.TOKEN_ADMIN_ID_COOKIE); 
					var pwd = CookieUtil.read(config.TOKEN_ADMIN_PWD_COOKIE);
					if (id != '' && pwd != '') {
		    			CookieUtil.write(config.TOKEN_ADMIN_ID_COOKIE, id);
    					CookieUtil.write(config.TOKEN_ADMIN_PWD_COOKIE, pwd);
    					CookieUtil.write(config.TOKEN_ACCOUNT_COOKIE, CookieUtil.read(config.TOKEN_ACCOUNT_COOKIE));
					}
					return true;
				}.bind(this)
			});
			
			$( document ).ajaxStart(function() {
				$( "#spinner" ).show();
			});			
			$( document ).ajaxStop(function() {
				$( "#spinner" ).hide();
			});			
			$('.back').on('click', function(event) {
	            window.history.back();
	            return false;
	        });
	        this.on("route:admin", function() {
	        	this.page = new AdminView();
	        	this.page.render();
		        $('body').append($(this.page.el));
	        });
			Backbone.history.start();
		},	
		routes: {
			'': 'admin'
		}
	});
	
	return Router;
});
