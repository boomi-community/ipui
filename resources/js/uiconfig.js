// Copyright (c) 2014 Dell Boomi, Inc.

define(function() {
	var url = "..";
	return {
		baseUrl: function() {
			return url;
		},
		wsUrl: function() {
			var wsUrl;
			if (window.location.protocol === "https:") {
				wsUrl = "wss";
			} else {
				wsUrl = "ws";
			}
			wsUrl += '://' + window.location.host+window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/'))+"/";
			return wsUrl;
		},
		setBaseUrl: function(newurl) {
			url = newurl;
		},
		TOKEN_ADMIN_ID_COOKIE: "BOOMI_IPUI_ADMIN_ID",
		TOKEN_ADMIN_PWD_COOKIE: "BOOMI_IPUI_ADMIN_PWD",
		TOKEN_ACCOUNT_COOKIE: "TOKEN_ACCOUNT_COOKIE",
		TOKEN_ID_COOKIE: "BOOMI_IPUI_TOKEN_ID",
		TOKEN_SECRET_COOKIE: "BOOMI_IPUI_TOKEN_SECRET"
	}
});